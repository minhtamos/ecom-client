module.exports = {
  purge: [],
  theme: {
    extend: {
      colors: {
        "charcoal-grey": "#3d3d3f",
        "charcoal-grey-80": "rgba(61, 61, 63, 0.8)",
        "dark-grey": "#202124",
        "white-two": "#f6f6f6",
        "white-three": "#ededed",
        "white-four": "#d4d3d3",
        "pea-green": "#82bf11",
        "strawberry": "#f63f45",
        "pumpkin-orange": "#ff7413",
        "bright-orange": "#ff6900",
        "apricot": "#ffc371",
        "greyish-brown": "#4d4d4d",
        "pinkish-grey": "#cccccc",
        "greyish": "#acacac",
        "cornflower-50": "#5f6dff",
        "greyish-two": "#b7b7b7",
        "pale-orange": "#ffa15f",
        "black-grey": "#808080",
        "white": "#ffffff",
        "purple-btn": "#5f6dff",
        "fb": "#fbfbfb",
        "fa": "#fafafa",
        "f9": "#f9f9f9",
        "box": "#808080",
        "cancel": "#f05d62"
      },
      backgroundColor: (theme) => ({
        ...theme("colors"),
        "checkout-btn": "#ff5f6d",
        "input-error": "rgba(246, 63, 69, 0.04)",
        "pending": "#fbba4e"
      }),
      borderColor: (theme) => ({
        ...theme("colors"),
        "line": "#979797",
        "ec": "#ececec",
        "ea": "#eaeaea"
      }),
      inset: {
        'badge': '-8px',
        'nav': '2rem',
        'cart': '1.2rem',
        'sm': '0.2rem'
      },
      spacing: {
        '72': '18rem',
        '84': '21rem',
        '92': '23rem',
        '96': '24rem',
      },
      height: {
        "cate-banner": "22rem",
        "banner": "29rem",
        "cart-image": "5rem",
        "detail-img": "30rem",
        "detail-sub-img": "6rem",
        "add-pro-img": "16rem",
        "side-others": "7rem",
        "others": "10rem"
      },
      width: {
        "cart": "10rem",
        "cart-image": "4rem",
        "modal": "28rem",
        "detail-img": "20rem",
        "detail-sub-img": "5rem",
        "account-form": "25rem",
        "admin-navs": "16rem",
        "add-pro-img": "11rem",
      },
      fontSize: {
        '2xs': '.55rem'
      },
      opacity: {
        '40': '.4',
        '80': '.8'
      }
    },
  },
  variants: {
    display: ["group-hover"],
    translate: ["group-hover"],
    opacity: ["group-hover"],
    backgroundColor: ['hover', 'focus', 'active'],
    cursor: ['hover'],
    borderColor: ['hover'],
    pointerEvents: ['hover', 'group-hover'],
    translate: ['group-hover']
  },
  plugins: [],
};
