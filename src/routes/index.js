import { AppRoute } from "./AppRoute";
import { AccountRoute } from "./AccountRoute";
import { AdminRoute } from "./AdminRoute";

export { AppRoute, AccountRoute, AdminRoute };
