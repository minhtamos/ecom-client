import React from "react";
import { Switch, Route } from "react-router-dom";
// screens
import { Home, ProductsDisplayer, ProductDetail, Account } from "../screens";
// main components
import { Header } from "../components/Header";
import { Nav } from "../components/Nav";
import { Footer } from "../components/Footer";
import { CartDetail } from "../screens/CartDetail";
import { Orders } from "../screens/Orders";

export const HomeRoute = () => {
  return (
    <>
      <Header />
      <Nav />
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/list" component={ProductsDisplayer} exact />
        <Route path="/detail/:friendlyUrl" component={ProductDetail} exact />
        <Route path="/cart" component={CartDetail} exact />
        <Route path="/account" component={Account} />
        <Route path="/orders" component={Orders} exact/>
      </Switch>
      <Footer />
    </>
  );
};
