import React from "react";
import { Switch, Route } from "react-router-dom";
import { AccountSetting, AccountChangePassword } from "../components/Account";

export const AccountRoute = () => {
  return (
    <Switch>
      <Route path="/account/setting" component={AccountSetting} exact />
      <Route path="/account/password" component={AccountChangePassword} exact />
    </Switch>
  );
};
