import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { HomeRoute } from "./HomeRoute";
import { AdminLogin } from "../screens";
import { Admin } from "../screens/Admin";

export const AppRoute = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/admin/login" component={AdminLogin} exact />
        <Route path="/admin" component={Admin} />
        <Route path="/" component={HomeRoute} />
      </Switch>
    </BrowserRouter>
  );
};
