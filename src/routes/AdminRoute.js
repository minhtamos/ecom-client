import React from "react";
import { Switch, Route } from "react-router";
import {
  AdminOrders,
  AdminProducts,
  AdminProductForm,
  AdminCategory,
} from "../components/Admin";

export const AdminRoute = () => {
  return (
    <Switch>
      <Route path="/admin/orders" component={AdminOrders} exact />
      <Route path="/admin/products" component={AdminProducts} exact />
      <Route path="/admin/category" component={AdminCategory} exact />
      <Route path="/admin/products/add" component={AdminProductForm} exact />
      <Route
        path="/admin/products/edit/:proUrl"
        component={AdminProductForm}
        exact
      />
    </Switch>
  );
};
