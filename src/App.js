import React from "react";
import Modal from "react-modal";
import { Provider } from "react-redux";
//store
import {rootStore} from "./stores/rootStore";
//routes
import { AppRoute } from "./routes/AppRoute";
//app css
import "./tailwind-o.css";
import "./styles/font.css";
import "./styles/global.css";
import "./styles/custom.css";;

// bind modal to appElement
Modal.setAppElement("#root"); 

function App() {
  return (
    <Provider store={rootStore}>
      <AppRoute/>
    </Provider>
  );
}

export default App;
