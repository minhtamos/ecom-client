import { cateBanners } from "./cateBanners";
import { emailReg } from "./emailReg";
import { footerNavs } from "./footerNavs";
import { navs } from "./navs";
import { sizesDefault } from "./sizes";
import { subNavs } from "./subNavs";
import { colors } from "./colors";
import { brands } from "./brands";
import { customModal } from "./customModal";
import { days, months } from "./date";
import { categories } from "./categories";
import { characters } from "./characters";
import { priceRange } from "./priceRange";

export {
  cateBanners,
  emailReg,
  footerNavs,
  navs,
  sizesDefault,
  subNavs,
  colors,
  brands,
  customModal,
  months,
  days,
  categories,
  characters,
  priceRange,
};
