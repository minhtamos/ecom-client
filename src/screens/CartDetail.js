import React, { useState } from "react";
import { SelectQuantity } from "../components/common";
import { useSelector, useDispatch } from "react-redux";
import { cartActionType } from "../stores/cart/cartActionType";
import Swal2 from "sweetalert2";
import { DotLoader } from "react-spinners";
import { Link } from "react-router-dom";

const CartDetailStyles = {
  container: {
    minHeight: "70vh",
  },
  img: {
    height: "113px",
    width: "80px",
  },
  color: {
    height: "30px",
    width: "30px",
  },
};

const calCartTotalPrice = (cart) => {
  let sum = 0;
  for (const item of cart) {
    sum += item.quantity * item.price;
  }
  return sum;
};

const calCartTotalPieces = (cart) => {
  let sum = 0;
  for (const item of cart) {
    sum += item.quantity;
  }
  return sum;
};

export const CartDetail = () => {
  const cart = useSelector((state) => state.cart);
  const userEmail = useSelector((state) => state.user.email);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);

  const checkOut = async () => {
    //add order
    setLoading(true);

    if (cart.length !== 0) {
      Swal2.fire({
        title: "Are you sure?",
        text: "Make sure you want to order!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, order!",
      }).then(async (result) => {
        if (result.value) {
          //update product quantities
          const bodyUQ = {
            //UQ = update quantities
            sizes: cart.map((item) => item.size),
            quantities: cart.map((item) => item.quantity),
            urls: cart.map((item) => item.url),
          };
          const resUQ = await fetch("/api/product/order", {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(bodyUQ),
          });
          const resUQJSON = await resUQ.json();
          if (!resUQJSON.success) {
            Swal2.fire({
              title: "Can not order",
              icon: "error",
              html:
                `<b><a href="/detail/${resUQJSON.proUrl}" style={{textDecoration: 'underline'}}>${resUQJSON.proName}</a></b>` +
                ` run out of stock, only ${resUQJSON.left} left`,
            });
          } else {
            const orders = cart.map((item) => {
              item.userEmail = userEmail;
              item.orderedAt = new Date();
              item.status = "pending";
              return item;
            });
            const resOr = await fetch("/api/order/many", {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(orders),
            });

            const resOrJSON = await resOr.json();
            if (resOrJSON.success) {
              localStorage.removeItem("cart");
              dispatch({ type: cartActionType.removeCart });
              Swal2.fire("Order successful!");
            } else {
              Swal2.fire({
                icon: "error",
                title: "Can not order!",
                text: resOrJSON.error.name || "Something wrong with server",
              });
            }
          }
        }
      });
    } else {
      Swal2.fire({
        icon: "error",
        title: "Can not order!",
        text: "Cart is empty",
      });
    }
    setLoading(false);
  };
  return (
    <div className="p-16 text-dark-grey cart-detail" style={{}}>
      <h1 className="font-medium text-2xl">My Bag</h1>
      <div className="flex justify-between">
        <table className="w-full">
          <thead>
            <tr className="border-b border-brown-grey">
              <th className="">Product</th>
              <th className="">Color</th>
              <th className="">Size</th>
              <th className="">Quantity</th>
              <th className="">Amount</th>
            </tr>
          </thead>
          {cart.length === 0 ? (
            <tbody>
              <tr className="border-b border-brown-grey">
                <td colSpan={5}>
                  <h3 className="text-red-600">No item in cart</h3>
                </td>
              </tr>
            </tbody>
          ) : (
            cart.map((item, index) => {
              return (
                <tbody
                  key={`${item.name}-${item.size}-${item.color}`}
                  className="border-b border-brown-grey"
                >
                  <tr>
                    <td>
                      <div className="flex">
                        <img
                          src={item.image}
                          style={CartDetailStyles.img}
                          className="object-contain"
                          alt="thump"
                        />
                        <div className="flex flex-col justify-between ml-2 mr-20">
                          <Link className="font-medium" to={`/detail/${item.url}`}>{item.name}</Link>
                          <div className="flex text-xs">
                            <Link to={`/detail/${item.url}`}>Change</Link>
                            <div className="border-r border-line mx-2"></div>
                            <button
                              onClick={() =>
                                dispatch({
                                  type: cartActionType.deleteIndex,
                                  payload: index,
                                })
                              }
                            >
                              Remove
                            </button>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <div className="flex items-center justify-center">
                        <div
                          style={Object.assign({}, CartDetailStyles.color, {
                            backgroundColor: item.color,
                          })}
                          className="rounded-full"
                        ></div>
                      </div>
                    </td>
                    <td>
                      <div className="flex items-center uppercase text-lg font-medium justify-center">
                        {item.size}
                      </div>
                    </td>
                    <td>
                      <div className="flex justify-center items-center">
                        <SelectQuantity
                          selectedQuantity={item.quantity}
                          max={item.max}
                          onChange={(value) => {
                            dispatch({
                              type: cartActionType.setQuantity,
                              payload: { index: index, quantity: value },
                            });
                          }}
                        />
                      </div>
                    </td>
                    <td>
                      <p className="text-dark-grey text-base font-medium">
                        ${item.price}
                      </p>
                    </td>
                  </tr>
                </tbody>
              );
            })
          )}
        </table>
        <div className="ml-4 w-modal">
          <h2 className="text-dark-grey font-bold">Total</h2>
          <div className="p-4 bg-f9 font-medium">
            <div className="flex justify-between">
              <p>Shipping & Handling:</p>
              <p>Free</p>
            </div>
            <div className="flex justify-between">
              <p>Total product:</p>
              <p>{calCartTotalPieces(cart)}</p>
            </div>
            <div className="border-b border-brown-grey my-4"></div>
            <div className="flex justify-between font-bold">
              <p>Subtotal</p>
              <p>${calCartTotalPrice(cart).toFixed(2)}</p>
            </div>
          </div>
          <button
            className="text-white text-lg bg-checkout-btn w-full py-4 font-bold shadow-lg focus:outline-none active:bg-red-500 hover:bg-red-600"
            onClick={checkOut}
          >
            Check out
          </button>
          {loading ? <DotLoader color="#ff6900" /> : ""}
        </div>
      </div>
    </div>
  );
};
