import React from "react";
import { HomeBanner, HomeCateBanners } from "../components/Home";

export const Home = () => {
  return (
    <div>
      <HomeBanner />
      <HomeCateBanners />
    </div>
  );
};
