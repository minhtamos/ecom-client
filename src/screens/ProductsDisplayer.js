import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router";
import { useQuery } from "../hooks";
import {
  ProductsCategorySelector,
  ProductsListing,
  ProductSortBy,
} from "../components/ProductsDisplayer";
import { ArrowIcon } from "../components/icons/ArrowIcon";
import { FilterOption } from "../components/ProductsDisplayer/ProductsFilter/FilterOption";
import { sizesDefault, colors, brands } from "../constants";
import {
  FilterSize,
  FilterColor,
  FilterBrand,
  FilterPrice,
  FilterAvailable,
} from "../components/ProductsDisplayer/ProductsFilter/FilterOptions";

export const ProductsDisplayer = () => {
  const query = useQuery();
  const history = useHistory();
  const location = useLocation();
  //query
  const page = query.get("page") || "1";
  const sizesQuery = query.get("sizes") ? query.get("sizes").split(",") : [];
  const colorsQuery = query.get("colors") ? query.get("colors").split(",") : [];
  const brandsQuery = query.get("brands") ? query.get("brands").split(",") : [];
  const availableQuery = query.get("available")
    ? query.get("available").split(",")
    : [];
  const categoryQuery = query.get("category") ? query.get("category") : "All";
  const characterQuery = query.get("character") ? query.get("character") : "";
  const searchQuery = query.get("search") ? query.get("search") : "";

  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [pages, setPages] = useState("1");
  //filter state
  const [selectedSizes, setSizes] = useState(sizesQuery);
  const [selectedColors, setColors] = useState(colorsQuery);
  const [selectedBrands, setBrands] = useState(brandsQuery);
  const [selectedAvailable, setAvailable] = useState(availableQuery);
  const [selectedCategory, setCategory] = useState(categoryQuery);
  const [priceFilter, setPriceFilter] = useState({
    min: parseInt(query.get("min")) || 0,
    max: parseInt(query.get("max")) || 300,
  });
  const [selectedPrice, setPrice] = useState({
    min: parseInt(query.get("min")) || 0,
    max: parseInt(query.get("max")) || 300,
  });
  const [selectedSortBy, setSortBy] = useState([]);

  const createQueryUrl = () => {
    const params = { page: page };
    if (searchQuery !== "") {
      params.search = searchQuery;
    }
    if (selectedCategory !== "") {
      params.category = selectedCategory;
    }
    if (characterQuery !== "") {
      params.character = characterQuery;
    }
    if (selectedSizes.length !== 0) {
      params.sizes = selectedSizes;
    }
    if (selectedColors.length !== 0) {
      params.colors = selectedColors;
    }
    if (selectedBrands.length !== 0) {
      params.brands = selectedBrands;
    }
    if (selectedAvailable.length !== 0) {
      params.available = selectedAvailable;
    }
    if (selectedSortBy.length !== 0) {
      const asc = [];
      const desc = [];
      for (let i = 0; i < selectedSortBy.length; i++) {
        const sb = selectedSortBy[i].split("-");
        if (sb.length === 2) {
          if (sb[1] === "asc") {
            asc.push(sb[0]);
          }
          if (sb[1] === "desc") {
            desc.push(sb[0]);
          }
        }
      }
      if (asc.length !== 0) {
        params.asc = asc;
      }
      if (desc.length !== 0) {
        params.desc = desc;
      }
    }
    params.max = selectedPrice.max;
    params.min = selectedPrice.min;
    const queryUrl = Object.keys(params)
      .map(function (k) {
        return (
          encodeURIComponent(k) + "=" + encodeURIComponent(params[k].toString())
        );
      })
      .join("&");
    return "?" + queryUrl;
  };

  useEffect(() => {
    fetch(`/api/product/pagination/card${location.search}`).then((res) => {
      res.json().then((data) => {
        if (data.success) {
          if (pages !== data.pages) {
            setPages(data.pages);
          }
          setProducts(data.products);
        }
        setLoading(false);
      });
    }); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.search]); 

  useEffect(() => {
    const url = "/list" + createQueryUrl();
    history.push(url);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    selectedSizes,
    selectedColors,
    selectedBrands,
    selectedAvailable,
    selectedPrice,
    selectedCategory,
    selectedSortBy,
  ]);

  const increasePage = () => {
    if (parseInt(page) < parseInt(pages)) {
      history.push(`/list?page=${parseInt(page) + 1}`);
    }
  };

  const decreasePage = () => {
    if (parseInt(page) > 1) {
      history.push(`/list?page=${parseInt(page) - 1}`);
    }
  };

  return (
    <div className="px-16">
      <p className="text-center my-4">Ladies / Dresses</p>
      <div className="flex">
        <div className="w-64 mb-16">
          <ProductsCategorySelector
            selectedCategory={selectedCategory}
            setCategory={setCategory}
            character={characterQuery}
          />
          <aside className="mt-8">
            <p className="font-bold mb-8">Filter</p>
            <FilterOption title="Size">
              <FilterSize
                sizes={sizesDefault}
                selectedSizes={selectedSizes}
                setSizes={setSizes}
              />
            </FilterOption>
            <FilterOption title="Color">
              <FilterColor
                colors={colors}
                selectedColors={selectedColors}
                setColors={setColors}
              />
            </FilterOption>
            <FilterOption title="Brand">
              <FilterBrand
                brands={brands}
                selectedBrands={selectedBrands}
                setBrands={setBrands}
              />
            </FilterOption>
            <FilterOption title="Price">
              <FilterPrice
                priceFilter={priceFilter}
                setPriceFilter={setPriceFilter}
                setPrice={setPrice}
              />
            </FilterOption>
            <FilterOption title="Available">
              <FilterAvailable
                selectedAvailable={selectedAvailable}
                setAvailable={setAvailable}
              />
            </FilterOption>
          </aside>
        </div>
        <div className="w-full">
          <div className="flex justify-between w-full items-center text-xs">
            <ProductSortBy
              selectedSortBy={selectedSortBy}
              setSortBy={setSortBy}
            />
            <div className="grid grid-cols-3 gap-4 items-center my-4">
              <button className="focus:outline-none" onClick={decreasePage}>
                <ArrowIcon left />
              </button>
              <p className="font-medium">{`${page}/${pages}`}</p>
              <button className="focus:outline-none" onClick={increasePage}>
                <ArrowIcon right />
              </button>
            </div>
          </div>
          {loading ? (
            "loading"
          ) : products.length === 0 ? (
            <p className="text-center text-strawberry min-h-text text-xs block italic mt-8">
              No products
            </p>
          ) : (
            <ProductsListing products={products} />
          )}
        </div>
      </div>
    </div>
  );
};
