import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { userActionType } from "../stores/user/userActionType";
import { Input, FormBtn } from "../components/form";
import "./AdminLogin.css";

export const AdminLogin = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isErr, setIsErr] = useState(false);

  const login = async (e) => {
    e.preventDefault();
    const body = {
      email,
      password,
    };
    const res = await fetch("/api/user/login/admin", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
    const resJSON = await res.json();
    if (resJSON.success) {
      const { user } = resJSON;
      dispatch({
        type: userActionType.logIn,
        payload: { ...user, authenticated: true },
      });
      history.push("/admin")
    } else {
      setIsErr(true);
    }
  };

  return (
    <div className="admin-login-background h-screen flex justify-center items-center">
      <form
        className="bg-charcoal-grey-80 flex flex-col items-center w-account-form px-8 py-8 rounded-sm"
        onSubmit={login}
      >
        <h1 className="text-3xl font-bold text-pale-orange">Log in</h1>
        <p className="text-center text-strawberry min-h-text text-xs block">
          {isErr ? "Your e-mail/password is invalid!" : ""}
        </p>
        <Input
          type="email"
          title="EMAIL"
          error={{}}
          labelClass="text-greyish"
          bgClass="bg-white"
          borderClass="border-white-three"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Input
          type="password"
          title="PASSWORD"
          error={{}}
          labelClass="text-greyish"
          bgClass="bg-white"
          borderClass="border-white-three"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <FormBtn title="Log in" />
        <button className="text-white font-semibold text-xs mt-2">
          Forgot password
        </button>
      </form>
    </div>
  );
};
