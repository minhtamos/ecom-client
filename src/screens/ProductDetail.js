import React, { useState, useEffect } from "react";
import Swal2 from "sweetalert2";
import { useDispatch, useSelector } from "react-redux";
import {
  ProductReviews,
  ProductOthers,
  SelectColor,
  SelectSize,
  SubImg,
  ProductSideOthers,
} from "../components/ProductDetail";
import { StarsRating, SelectQuantity } from "../components/common";
import { useParams } from "react-router";
import { cartActionType } from "../stores/cart/cartActionType";
import ImgPlaceHolder from "../images/500.png";

const ProductDetailStyles = {
  img: { width: "379px", height: "537px" },
  button: {
    width: "429px",
    height: "50px",
  },
};

export const ProductDetail = () => {
  const dispatch = useDispatch();
  const params = useParams();
  //UI state
  const [loading, setLoading] = useState(true);
  const [mainImg, setMainImg] = useState("");
  const [product, setProduct] = useState(null);
  const [quantityLeft, setQuantityLeft] = useState(0);
  //cart state
  const [selectedQuantity, setQuantity] = useState(1);
  const [selectedSize, setSelectedSize] = useState("");
  const [selectedColor, setSelectedColor] = useState("");

  const cart = useSelector((state) => state.cart);

  const addToCart = () => {
    const newCart = {
      name: product.name,
      url: product.friendlyUrl,
      color: selectedColor,
      size: selectedSize,
      quantity: selectedQuantity,
      price: product.price,
      image: product.images[0],
      max: quantityLeft,
    };

    const existedItemIndex = cart.findIndex((product) => {
      return (
        product.name === newCart.name &&
        product.size === newCart.size &&
        product.color === newCart.color
      );
    });
    if (existedItemIndex !== -1) {
      if (cart[existedItemIndex].quantity + newCart.quantity >= quantityLeft) {
        Swal2.fire({
          icon: "error",
          title: `We only have ${quantityLeft} left ^^`,
        });
      } else {
        dispatch({ type: cartActionType.increaseQuantity, payload: newCart });
        Swal2.fire("Added to cart!");
      }
    } else {
      dispatch({ type: cartActionType.addCart, payload: newCart });
      Swal2.fire("Added to cart!");
    }
  };

  useEffect(() => {
    const { friendlyUrl } = params;

    fetch(`/api/product/detail/${friendlyUrl}`).then((res) => {
      res.json().then((data) => {
        console.log(data);
        if (data.success) {
          setProduct(data.product);
          setSelectedSize(data.product.sizes[0]);
          setSelectedColor(data.product.colors[0]);
          setMainImg(data.product.images[0]);
          setLoading(false);
        } else {
          setLoading(false);
        }
      });
    });

    window.scrollTo(0, 0);
  }, [params]);

  useEffect(() => {
    if (product) {
      const { sizes, quantities, solds } = product;
      const index = sizes.indexOf(selectedSize);
      setQuantityLeft(quantities[index] - solds[index]);
    }
  }, [product, loading, selectedSize]);

  return (
    <div className="px-16">
      <p className="text-center mb-4 mt-12">
        Ladies / Dresses / Collete Stretch Linen Minidress
      </p>
      {loading ? (
        <h1 className="text-lg italic text-center mt-16 mb-64">Loading</h1>
      ) : !product ? (
        <h1 className="text-strawberry text-lg italic text-center mt-16 mb-64">
          Can not load product
        </h1>
      ) : (
        <>
          <div className="flex my-8 justify-between">
            <div className="flex">
              <SubImg images={product.images} setMainImg={setMainImg} />
              <div className="w-detail-img h-detail-img overflow-hidden">
                <img
                  src={mainImg}
                  alt="mainImage"
                  placeholder={ImgPlaceHolder}
                  style={ProductDetailStyles.img}
                  className="object-cover image-zoom"
                />
              </div>
              <div className="mx-8 text-dark-grey font-hairline">
                <h1 className="font-medium text-2xl">{product.name}</h1>
                <div className="flex text-xl text-dark-grey">
                  <h3 className=" font-light">${product.price}</h3>
                  <div className="mx-4 border-l border-white-four"></div>
                  <h3
                    style={ProductDetailStyles.price}
                  >{`${quantityLeft} left`}</h3>
                </div>
                <div className="flex my-4 text-dark-grey">
                  <StarsRating rating={4} />
                  <div className="mx-2 border-l border-white-four"></div>
                  <p className="text-xs">0 Review</p>
                </div>
                <div className="my-4">
                  <h5 className="font-medium">Size</h5>
                  <SelectSize
                    sizes={product.sizes}
                    selectedSize={selectedSize}
                    setSelectedSize={setSelectedSize}
                  />
                </div>
                <div className="my-4">
                  <h5 className="font-medium">Color</h5>
                  <SelectColor
                    oneline
                    colors={product.colors}
                    selectedColor={selectedColor}
                    setSelectedColor={setSelectedColor}
                  />
                </div>
                <div className="flex items-center">
                  <h5 className="font-medium">Quantity</h5>
                  <SelectQuantity
                    selectedQuantity={selectedQuantity}
                    onChange={(value) => setQuantity(value)}
                    max={quantityLeft}
                  />
                </div>
                {quantityLeft === 0 ? (
                  <button
                    className="shadow-lg font-bold mt-8 focus:outline-none bg-gray-200 cursor-default pointer-events-none"
                    style={ProductDetailStyles.button}
                    onClick={addToCart}
                  >
                    Out of stock
                  </button>
                ) : (
                  <button
                    className="shadow-lg text-white font-bold mt-8 focus:outline-none bg-purple-btn hover:bg-indigo-800 active:bg-indigo-400"
                    style={ProductDetailStyles.button}
                    onClick={addToCart}
                  >
                    Add to cart
                  </button>
                )}
                <div className="border-b border-white-four mt-4"></div>
                <p className="mt-4 font-medium">{product.description}</p>
              </div>
            </div>
            <ProductSideOthers />
          </div>
          <ProductReviews reviews={product.reviews} proName={product.name} />
          <ProductOthers />
        </>
      )}
    </div>
  );
};
