import React from "react";
import { NavLink } from "react-router-dom";
import { AccountRoute } from "../routes";

export const Account = () => {
  return (
    <div className="p-16 flex">
      <aside>
        <h1 className="text-xl font-medium">My Account</h1>
        <NavLink
          to="/account/setting"
          className="block mt-8"
          activeClassName="text-bright-orange"
        >
          Account setting
        </NavLink>
        <NavLink
          to="/account/password"
          className="block mt-4"
          activeClassName="text-bright-orange"
        >
          Change password
        </NavLink>
      </aside>
      <AccountRoute/>
    </div>
  );
};
