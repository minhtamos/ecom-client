import React, { useEffect } from "react";
import { NavLink, useLocation, useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import {
  LogoIcon,
  OverviewIcon,
  OrdersIcon,
  ProductsIcon,
  PaymentIcon,
  PromotionIcon,
  SettingIcon,
} from "../components/icons";
import { AdminHeader } from "../components/Admin";
import { AdminRoute } from "../routes/AdminRoute";

export const Admin = () => {
  const location = useLocation();
  const history = useHistory();
  const path = location.pathname.split("/")[2];
  const user = useSelector((state) => state.user);
  useEffect(() => {
    if(!user.authenticated || user.role !== "admin"){
      history.replace("/admin/login")
    }
  });

  return (
    <div className="flex bg-white-two min-h-screen">
      <nav className="w-admin-navs flex flex-col min-h-screen bg-aqua shadow">
        <div className="py-8 flex justify-center">
          <LogoIcon />
        </div>
        <NavLink
          to="/admin/overview"
          className="flex items-center py-3 pl-4 border-l-8 border-transparent"
          activeClassName="text-pale-orange border-l-8 border-pale-orange"
        >
          <OverviewIcon active={path === "overview"} />
          <h2 className="font-medium ml-4">Overview</h2>
        </NavLink>
        <NavLink
          to="/admin/orders"
          className="flex items-center py-3 pl-4 border-l-8 border-transparent"
          activeClassName="text-pale-orange border-l-8 border-pale-orange"
        >
          <OrdersIcon active={path === "orders"} />
          <h2 className="font-medium ml-4">Orders</h2>
        </NavLink>
        <NavLink
          to="/admin/products"
          className="flex items-center py-3 pl-4 border-l-8 border-transparent"
          activeClassName="text-pale-orange border-l-8 border-pale-orange"
        >
          <ProductsIcon active={path === "products"} />
          <h2 className="font-medium ml-4">Products</h2>
        </NavLink>
        <NavLink
          to="/admin/category"
          className="flex items-center py-3 pl-4 border-l-8 border-transparent"
          activeClassName="text-pale-orange border-l-8 border-pale-orange"
        >
          <PaymentIcon active={path === "category"} />
          <h2 className="font-medium ml-4">Category</h2>
        </NavLink>
        <NavLink
          to="/admin/promotions"
          className="flex items-center py-3 pl-4 border-l-8 border-transparent"
          activeClassName="text-pale-orange border-l-8 border-pale-orange"
        >
          <PromotionIcon active={path === "promotions"} />
          <h2 className="font-medium ml-4">Promotions</h2>
        </NavLink>
        <NavLink
          to="/admin/setting"
          className="flex items-center py-3 pl-4 border-l-8 border-transparent"
          activeClassName="text-pale-orange border-l-8 border-pale-orange"
        >
          <SettingIcon active={path === "setting"} />
          <h2 className="font-medium ml-4">Setting</h2>
        </NavLink>
      </nav>
      <div className="w-full p-4">
        <AdminHeader title={path} />
        <AdminRoute />
      </div>
    </div>
  );
};
