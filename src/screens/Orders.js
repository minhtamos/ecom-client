import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import classnames from "classnames";
import Swal2 from "sweetalert2";
import { DropDownIcon } from "../components/icons";
import { getDateFormated } from "../utils";

export const Orders = () => {
  const userEmail = useSelector((state) => state.user.email);
  const [orders, setOrders] = useState([]);

  const getData = () => {
    if (userEmail !== "") {
      fetch(`/api/order?userEmail=${userEmail}`).then((res) =>
        res.json().then((resJSON) => {
          if (resJSON.success) {
            setOrders(resJSON.orders);
          }
        })
      );
    }
  };

  const handleCancelClick = async (order) => {
    Swal2.fire({
      title: "Are you sure?",
      text: "Make sure you want to delete order!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete!",
    }).then(async (result) => {
      if (result.value) {
        fetch(`/api/order/status/canceled?id=${order._id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
        }).then((res) => {
          res.json().then((resJSON) => {
            if (resJSON.success) {
              const body = {
                url: order.url,
                size: order.size,
                quantity: order.quantity,
              };
              fetch(`/api/product/order/cancel`, {
                method: "PUT",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify(body),
              }).then((res) => {
                if (res.status === 200) {
                  getData();
                  Swal2.fire("Canceled order");
                }
              });
            }
          });
        });
      }
    });
  };

  useEffect(() => {
    getData();
  }, []); //eslint-disable-line react-hooks/exhaustive-deps

  const getDetail = (order) => {
    return `${order.name} (${order.size.toUpperCase()}) x ${order.quantity}`;
  };

  const getTotal = (order) => {
    return order.price * order.quantity;
  };

  return (
    <div className="min-h-screen">
      <h1 className="mt-16 mb-8 text-center font-bold text-xl">
        Order checking
      </h1>
      {userEmail === "" ? (
        <div>
          <p className="text-center text-strawberry min-h-text text-xs block">
            Please login to see your orders
          </p>
        </div>
      ) : (
        <div>
          {orders.length === 0 ? (
            <p className="text-center text-strawberry min-h-text text-xs block">
              Empty orders, buy something ^^
            </p>
          ) : (
            <div>
              <table className="w-full admin-table">
                <thead>
                  <tr className="border-b border-white-three">
                    <th>ORDER ID</th>
                    <th>ORDER DATE</th>
                    <th>DETAIL</th>
                    <th>TOTAL ($)</th>
                    <th className="flex items-center">
                      STATUS <DropDownIcon />
                    </th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {orders.length === 0 ? (
                    <tr className="">
                      <td colSpan={5} className="text-strawberry italic">
                        No records
                      </td>
                    </tr>
                  ) : (
                    orders.map((order) => {
                      return (
                        <tr key={order._id}>
                          <td className="text-center text-charcoal-grey font-medium text-xs">
                            {order._id}
                          </td>
                          <td className="text-center text-charcoal-grey font-medium text-sm">
                            {getDateFormated(order.orderedAt)}
                          </td>
                          <td className="text-center text-charcoal-grey font-medium text-sm">
                            {getDetail(order)}
                          </td>
                          <td className="text-center text-charcoal-grey font-medium text-sm">
                            {getTotal(order)}
                          </td>
                          <td className="text-center text-charcoal-grey font-medium text-sm">
                            <div
                              className={classnames(
                                "text-white rounded-full bg-pea-green text-xss w-20 h-5 flex justify-center items-center",
                                {
                                  "bg-pending": order.status === "pending",
                                  "bg-pea-green": order.status === "completed",
                                  "bg-cancel": order.status === "canceled",
                                }
                              )}
                            >
                              {order
                                ? order.status.charAt(0).toUpperCase() +
                                  order.status.slice(1)
                                : "not string"}
                            </div>
                          </td>
                          <td>
                            {order.status !== "canceled" ? (
                              <button
                                className=" bg-red-600 text-white font-medium px-4 py-2 hover:bg-red-800"
                                onClick={() => handleCancelClick(order)}
                              >
                                Cancel
                              </button>
                            ) : (
                              <button
                                className=" bg-gray-800 text-white font-medium px-4 py-2 hover:cursor-auto"
                                disabled
                              >
                                Cancel
                              </button>
                            )}
                          </td>
                        </tr>
                      );
                    })
                  )}
                </tbody>
              </table>
            </div>
          )}
        </div>
      )}
    </div>
  );
};
