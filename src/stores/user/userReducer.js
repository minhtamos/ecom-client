import { userActionType } from "./userActionType";

const initUser = {};

export const userReducer = (state = initUser, action) => {
  switch (action.type) {
    case userActionType.logIn:
      return action.payload;
    case userActionType.logOut:
      return JSON.parse(JSON.stringify(initUser));
    case userActionType.changeInfo:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
