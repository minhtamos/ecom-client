export const userActionType = {
  logIn: "userActionType-logIn",
  logOut: "userActionType-logOut",
  changeInfo: "userActionType-changeInfo",
  changePass: "userActionType-changePass"
};
