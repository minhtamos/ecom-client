import { createStore } from "redux";
import { rootReducer } from "./rootReducer";
import jwt from "jsonwebtoken";

export const loadState = () => {
  try {
    const serializedState = {};
    const authToken = localStorage.getItem("authToken");
    if (authToken) {
      const user = jwt.verify(authToken, process.env.REACT_APP_JWT);
      serializedState.user = user;
    }
    const cart = JSON.parse(localStorage.getItem("cart"));
    if (cart) {
      serializedState.cart = cart;
    }
    if (serializedState.cart || serializedState.user) {
      return serializedState;
    } else {
      return undefined;
    }
  } catch (err) {
    return undefined;
  }
};

export const saveState = (state) => {
  try {
    const user = JSON.stringify(state.user);
    const cart = JSON.stringify(state.cart);
    localStorage.setItem("cart", cart);
    localStorage.setItem(
      "authToken",
      jwt.sign(user, process.env.REACT_APP_JWT)
    );
  } catch {
    // ignore write errors
  }
};

const rootStore = createStore(rootReducer, loadState());
rootStore.subscribe(() => {
  saveState(rootStore.getState());
});

export { rootStore };
