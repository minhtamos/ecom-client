export const cartActionType = {
  addCart: "cartActiveType-addCart",
  increaseQuantity: "cartActiveType-increaseQuantity",
  decreaseQuantity: "cartActiveType-decreaseQuantity",
  deleteIndex: "cartActiveType-deleteIndex",
  setCart: "cartActiveType-setCart",
  removeCart: "cartActiveType-removeCart",
  setQuantity: "cartActiveType-setQuantity",
};
