import { cartActionType } from "./cartActionType";

export const cartReducer = (state = [], action) => {
  switch (action.type) {
    case cartActionType.setCart:
      return action.payload;
    case cartActionType.addCart:
      return [...state, action.payload];
    case cartActionType.increaseQuantity:
      return [...state].map((item) => {
        if (item.name === action.payload.name) {
          item.quantity += action.payload.quantity;
        }
        return item;
      });
    case cartActionType.deleteIndex:
      return [...state].filter((value, index) => {
        return index !== action.payload;
      });
    case cartActionType.setQuantity:
      return [...state].map((item, index) => {
        if (index === action.payload.index) {
          item.quantity = action.payload.quantity;
        }
        return item;
      });
    case cartActionType.removeCart:
      return [];
    default:
      return state;
  }
};
