import React from "react";
import { StarIcon, StarActiveIcon } from "../icons";

export const StarsRating = ({ rating }) => {
  const stars = ["star1", "star2", "star3", "star4", "star5"];
  return (
    <div className="grid grid-cols-5 gap-2 w-24">
      {stars.map((star, index) => {
        return index + 1 <= rating ? (
          <StarActiveIcon key={`star-${star}`} />
        ) : (
          <StarIcon key={`star-${star}`} />
        );
      })}
    </div>
  );
};
