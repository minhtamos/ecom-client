import React from "react";
import { MinusIcon, PlusIcon } from "../icons";

export const SelectQuantity = ({ selectedQuantity, onChange, max }) => {
  return (
    <div className="py-2 flex justify-around items-center border border-white-four w-24 ml-4">
      <button
        className="focus:outline-none"
        onClick={() =>
          onChange(selectedQuantity - 1 < 1 ? 1 : selectedQuantity - 1)
        }
      >
        <MinusIcon />
      </button>
      <p className="font-medium">{selectedQuantity}</p>
      <button
        className="focus:outline-none"
        onClick={() =>
          onChange(selectedQuantity + 1 > max ? max : selectedQuantity + 1)
        }
      >
        <PlusIcon />
      </button>
    </div>
  );
};
