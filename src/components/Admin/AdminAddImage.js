import React, { useState, useEffect } from "react";
import { AddIcon } from "../icons";

export const AdminAddImage = ({ setImages, images }) => {
  const [file, setFile] = useState(null);

  useEffect(() => {
    if (file) {
      const formData = new FormData();
      formData.append("image", file);
      fetch(`/api/image`, {
        method: "POST",
        body: formData,
      }).then((res) => {
        res.json().then((resJSON) => {
          if (resJSON.success) {
            setImages([...images, resJSON.url]);
          }
        });
      });
    }// eslint-disable-next-line react-hooks/exhaustive-deps
  }, [file]);

  return (
    <div>
      <label className="h-add-pro-img w-add-pro-img block bg-white shadow mr-4 cursor-pointer">
        <input
          type="file"
          onChange={(e) => {
            if (e.target.files.length !== 0) {
              setFile(e.target.files[0]);
            }
          }}
          className="hidden"
        />
        <div className="w-full h-full flex flex-col text-greyish font-medium justify-center items-center">
          <p>Add photo</p>
          <AddIcon />
        </div>
      </label>
    </div>
  );
};
