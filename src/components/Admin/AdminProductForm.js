import React, { useState, useEffect } from "react";
import Swal2 from "sweetalert2";
import { AdminMulSelect } from "./AdminMulSelect";
import { AdminSelect } from "./AdminSelect";
import { Link, useLocation, useParams } from "react-router-dom";
import { AdminMulInput } from "./AdminMulInput";
import {
  brands,
  colors,
  categories,
  sizesDefault,
  characters,
} from "../../constants";
import { Close1 } from "../icons";
import { proNameToUrl } from "../../utils";
import { AdminAddImage } from "./AdminAddImage";

export const AdminProductForm = () => {
  const location = useLocation();
  const { proUrl } = useParams();
  const isEdit = location.pathname.split("/")[3] === "edit";
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [selectedCategories, setCategories] = useState([]);
  const [selectedBrand, setBrand] = useState(brands[0]);
  const [selectedCharacter, setCharacter] = useState(characters[0]);
  const [selectedSizes, setSizes] = useState([]);
  const [selectedColors, setColors] = useState([]);
  const [selectedQuantities, setQuantities] = useState([]);
  const [images, setImages] = useState([]);
  const [id, setId] = useState("");

  useEffect(() => {
    if (isEdit) {
      fetch(`/api/product/detail/${proUrl}`).then((res) => {
        res.json().then((resJSON) => {
          if (resJSON.success) {
            const { product } = resJSON;
            setName(product.name);
            setCategories(product.categories);
            setBrand(product.brand);
            setCharacter(product.character);
            setPrice(product.price);
            setSizes(product.sizes);
            setColors(product.colors);
            setQuantities(product.quantities);
            setDescription(product.description);
            setImages(product.images);
            setId(product._id);
          } else {
          }
        });
      });
    }
  }, [isEdit, proUrl]);

  const validated = () => {
    if (name.length === 0) {
      Swal2.fire({
        icon: "error",
        title: "Can not add product!",
        text: "Product name can not empty",
      });
      return false;
    }
    if (selectedCategories.length === 0) {
      Swal2.fire({
        icon: "error",
        title: "Can not add product!",
        text: "Product categories can not empty",
      });
      return false;
    }
    if (isNaN(parseFloat(price))) {
      Swal2.fire({
        icon: "error",
        title: "Can not add product!",
        text: "Product price must be a number",
      });
      return false;
    }
    if (selectedSizes.length === 0) {
      Swal2.fire({
        icon: "error",
        title: "Can not add product!",
        text: "Sizes can not empty",
      });
      return false;
    }
    if (selectedColors.length === 0) {
      Swal2.fire({
        icon: "error",
        title: "Can not add product!",
        text: "Colors can not empty",
      });
      return false;
    }
    if (selectedQuantities.length === 0) {
      Swal2.fire({
        icon: "error",
        title: "Can not add product!",
        text: "Quantites can not empty",
      });
      return false;
    }
    if (selectedSizes.length !== selectedQuantities.length) {
      Swal2.fire({
        icon: "error",
        title: "Can not add product!",
        text: "Each size must have one quantity",
      });
      return false;
    }
    if (images.length <= 0) {
      Swal2.fire({
        icon: "error",
        title: "Can not add product!",
        text: "Must choose at least one image",
      });
      return false;
    }
    return true;
  };

  const sentFormProduct = async (e) => {
    e.preventDefault();
    if (validated()) {
      const newProduct = {
        id,
        name,
        categories: selectedCategories,
        brand: selectedBrand,
        character: selectedCharacter,
        price: parseFloat(price),
        sizes: selectedSizes,
        colors: selectedColors,
        quantities: selectedQuantities,
        description: description,
        images: images,
        friendlyUrl: proNameToUrl(name),
      };
      const res = await fetch("/api/product", {
        method: isEdit ? "PUT" : "POST",
        body: JSON.stringify(newProduct),
        headers: {
          "Content-Type": "application/json",
        },
      });
      const resJSON = await res.json();
      if (resJSON.success) {
        Swal2.fire("success");
      } else {
        Swal2.fire("Can not complete the product form");
      }
    }
  };

  const deleteImage = async (image, index) => {
    const name = image.split("/")[3];
    const res = await fetch(`/api/image?name=${name}`, {
      method: "DELETE",
    });
    await res.json();
    //not care about server deleted the image or not :3
    setImages(images.filter((img, i) => i !== index));
  };

  return (
    <div>
      <h2>Products / {isEdit ? "Edit" : "Add"} product</h2>
      <div className="mt-16 pl-8 pr-20 text-xs">
        <div className="flex items-center">
          <h3 className="w-48 text-right text-dark-grey font-bold text-xs1">
            PHOTOS
          </h3>
          <div className="flex ml-4 w-full flex-wrap">
            {images.map((image, index) => {
              return (
                <div className="relative" key={`image-upload-${index}`}>
                  <img
                    src={image}
                    alt="pro-img"
                    className="h-add-pro-img w-add-pro-img mr-4 object-contain"
                  />
                  <button
                    className="absolute top-0 right-cart"
                    onClick={() => {
                      deleteImage(image, index);
                    }}
                  >
                    <Close1 />
                  </button>
                </div>
              );
            })}
            <AdminAddImage setImages={setImages} images={images} />
          </div>
        </div>
        <div className="flex items-center mt-8">
          <h3 className="w-48 text-right text-dark-grey font-bold text-xs1">
            NAME
          </h3>
          <div className="flex ml-4 w-full">
            <input
              className="h-10 w-full px-4 text-charcoal-grey font-medium shadow"
              placeholder="Product name"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
        </div>
        <div className="flex items-center mt-8">
          <h3 className="w-48 text-right text-dark-grey font-bold text-xs1">
            CATEGORIES
          </h3>
          <div className="flex ml-4 w-full">
            <AdminMulSelect
              options={categories}
              selectedOps={selectedCategories}
              setSelectedOps={setCategories}
              enableAddNew
              field="category"
            />
          </div>
        </div>
        <div className="flex items-center mt-8">
          <h3 className="w-48 text-right text-dark-grey font-bold text-xs1">
            BRAND
          </h3>
          <div className="flex ml-4 w-full">
            <AdminSelect
              options={brands}
              selectedOp={selectedBrand}
              setSelectedOp={setBrand}
            />
          </div>
        </div>
        <div className="flex items-center mt-8">
          <h3 className="w-48 text-right text-dark-grey font-bold text-xs1">
            CHARACTER
          </h3>
          <div className="flex ml-4 w-full">
            <AdminSelect
              options={characters}
              selectedOp={selectedCharacter}
              setSelectedOp={setCharacter}
            />
          </div>
        </div>
        <div className="flex items-center mt-8">
          <h3 className="w-48 text-right text-dark-grey font-bold text-xs1">
            PRICE ($)
          </h3>
          <div className="flex ml-4 w-full">
            <input
              className="h-10 w-full px-4 text-charcoal-grey font-medium shadow"
              type="number"
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </div>
        </div>
        <div className="flex items-center mt-8">
          <h3 className="w-48 text-right text-dark-grey font-bold text-xs1">
            SIZES
          </h3>
          <div className="flex ml-4 w-full">
            <AdminMulSelect
              options={sizesDefault}
              selectedOps={selectedSizes}
              setSelectedOps={setSizes}
            />
          </div>
        </div>
        <div className="flex items-center mt-8">
          <h3 className="w-48 text-right text-dark-grey font-bold text-xs1">
            COLORS
          </h3>
          <div className="flex ml-4 w-full">
            <AdminMulSelect
              options={colors}
              selectedOps={selectedColors}
              setSelectedOps={setColors}
            />
          </div>
        </div>
        <div className="flex items-center mt-8">
          <h3 className="w-48 text-right text-dark-grey font-bold text-xs1">
            QUANTITIES
          </h3>
          <div className="flex-col ml-4 w-full">
            <AdminMulInput
              selectedOps={selectedQuantities}
              setSelectedOps={setQuantities}
            />
          </div>
        </div>
        <div className="flex items-center mt-8">
          <h3 className="w-48 text-right text-dark-grey font-bold text-xs1">
            DESCRIPTION
          </h3>
          <div className="flex ml-4 w-full">
            <textarea
              rows={4}
              className="w-full px-4 py-2 text-charcoal-grey font-medium focus:outline-none shadow"
              placeholder="model, length..."
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </div>
        </div>
        <div className="flex justify-end mt-8 mb-16">
          <Link
            className="py-3 px-16 bg-white border border-white-three text-pale-orange font-semibold"
            to="/admin/products"
          >
            Cancel
          </Link>
          <button
            className="py-3 px-16 bg-pale-orange border border-white-three text-white font-semibold ml-4"
            onClick={sentFormProduct}
          >
            Complete
          </button>
        </div>
      </div>
    </div>
  );
};
