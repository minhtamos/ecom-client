import React, { useState, useEffect } from "react";
import { DropDownIcon, PlusWhiteIcon, ExportIcon } from "../icons";
import { useQuery } from "../../hooks";
import { AdminProductsRow } from "./AdminProductsRow";
import { AdminSearch } from "./AdminSearch";
import { AdminPagination } from "./AdminPagination";
import { Link, useHistory } from "react-router-dom";

export const AdminProducts = () => {
  const [products, setProducts] = useState([]);
  const [maxPage, setMaxPage] = useState(1);
  const [total, setTotal] = useState(1);
  const [search, setSearch] = useState("");
  const query = useQuery();
  const page = parseInt(query.get("page")) || 1;
  const history = useHistory();

  const getData = () => {
    fetch(`/api/product/pagination/table?page=${page}&search=${search}`).then((res) =>
      res.json().then((resJSON) => {
        if (resJSON.pages < page) {
          history.replace(`/admin/products?page=${resJSON.pages}`);
        }
        if (resJSON.products.length > 0) {
          setProducts(resJSON.products);
        }
        if (maxPage !== resJSON.pages) {
          setMaxPage(resJSON.pages);
        }
        if (total !== resJSON.total) {
          setTotal(resJSON.total);
        }
      })
    );
  };

  useEffect(() => {
    if (page < 1) {
      history.replace(`/admin/products?page=${1}`);
    } else {
      getData();
    }
  }, [page]); //eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className="w-full">
      <div className="flex justify-between mt-4">
        <div className="flex items-center">
          <h3 className="font-semibold text-greyish text-xs">SORT BY</h3>
          <div className="bg-white flex justify-center items-center ml-4">
            <select className="block py-3 px-4 appearance-none bg-white text-charcoal-grey flex justify-between focus:outline-none">
              <option>Date added</option>
              <option>Apples</option>
              <option>Bananas</option>
              <option>Grapes</option>
              <option>Oranges</option>
            </select>
            <DropDownIcon />
          </div>
        </div>
        <div className="flex items-center">
          <AdminSearch
            placeholder="Search product"
            search={search}
            setSearch={setSearch}
            onClick={getData}
          />
          <Link
            className="px-4 py-3 text-white bg-pale-orange mx-8 flex items-center rounded-sm font-semibold"
            to="/admin/products/add"
          >
            <PlusWhiteIcon /> Add product
          </Link>
          <button className="px-4 py-3 text-pale-orange bg-white flex items-center rounded-sm font-semibold">
            <ExportIcon /> Add product
          </button>
        </div>
      </div>
      <div className="w-full font-light text-greyish text-xs mt-8 bg-white shadow-lg inline-block px-4">
        <table className="w-full admin-table">
          <thead>
            <tr className="border-b border-white-three">
              <th>PRODUCTS</th>
              <th>SOLD</th>
              <th>DATE ADDED</th>
              <th>PROFIT ($)</th>
              <th></th>
            </tr>
          </thead>
          <tbody className="px-8">
            {products.length === 0 ? (
              <tr className="">
                <td colSpan={5} className="text-strawberry italic">
                  No records
                </td>
              </tr>
            ) : (
              products.map((product) => {
                return (
                  <AdminProductsRow
                    key={product._id}
                    product={product}
                    getData={getData}
                  />
                );
              })
            )}
          </tbody>
        </table>
        <div className="mt-16 flex py-4 justify-between">
          <p className="text-charcoal-grey font-medium">
            {`Show ${(page - 1) * 10 + 1} to ${page * 10} of ${total} entries`}
          </p>
          <AdminPagination curPage={page} maxPage={maxPage} nav="products" />
        </div>
      </div>
    </div>
  );
};
