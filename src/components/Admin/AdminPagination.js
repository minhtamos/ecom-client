import React from "react";
import { PrevIcon, NextIcon, FirstPageIcon, LastPageIcon } from "../icons";
import { useHistory } from "react-router-dom";

export const AdminPagination = ({ curPage, maxPage, nav }) => {
  const history = useHistory();
  const buttonClass =
    "border border-white-three w-8 h-8 flex justify-center items-center";

  return (
    <div className="flex">
      <button
        className={buttonClass}
        onClick={() => history.push(`/admin/${nav}?page=${1}`)}
      >
        <FirstPageIcon />
      </button>
      <button
        className={buttonClass}
        onClick={() => {
          if (curPage - 1 >= 0) {
            history.push(`/admin/${nav}?page=${curPage - 1}`);
          }
        }}
      >
        <PrevIcon />
      </button>
      {curPage - 1 > 0 ? (
        <button
          className="border border-white-three h-8 w-8 text-charcoal-grey bg-white flex justify-center items-center font-medium"
          onClick={() => history.push(`/admin/${nav}?page=${curPage - 1}`)}
        >
          {curPage - 1}
        </button>
      ) : (
        ""
      )}
      <button className="border border-white-three h-8 w-8 text-white bg-pale-orange flex justify-center items-center font-medium">
        {curPage}
      </button>
      {curPage + 1 <= maxPage ? (
        <button
          className="border border-white-three h-8 w-8 text-charcoal-grey bg-white flex justify-center items-center font-medium"
          onClick={() => history.push(`/admin/${nav}?page=${curPage + 1}`)}
        >
          {curPage + 1}
        </button>
      ) : (
        ""
      )}
      <button
        className={buttonClass}
        onClick={() => {
          if (curPage + 1 <= maxPage) {
            history.push(`/admin/${nav}?page=${curPage + 1}`);
          }
        }}
      >
        <NextIcon />
      </button>
      <button
        className={buttonClass}
        onClick={() => history.push(`/admin/${nav}?page=${maxPage}`)}
      >
        <LastPageIcon />
      </button>
    </div>
  );
};
