import React, { useState, useRef, useEffect } from "react";
import classnames from "classnames";
import { CloseIcon, DropDownIcon, AddIcon } from "../icons";
import { useOnClickOutSide } from "../../hooks";
import "./AdminMulSelect.css";

export const AdminMulSelect = ({
  options,
  selectedOps,
  setSelectedOps,
  enableAddNew,
  field,
}) => {
  const [openDropDown, setOpenDownDown] = useState(false);
  const [dropDownOps, setDropDownOps] = useState(options);
  const [newOp, setNewOp] = useState("");
  const ref = useRef();
  useOnClickOutSide(ref, () => {
    setOpenDownDown(false);
  });

  const addNewOp = async () => {
    if (field === "category") {
      const body = {
        name: newOp,
      };
      const res = await fetch(`/api/category`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
      const resJSON = await res.json();
      if (resJSON.success) {
        setDropDownOps([...dropDownOps, resJSON.category.name]);
      }
    }
  };

  useEffect(() => {
    if (field === "category") {
      fetch("/api/category").then((res) => {
        res.json().then((resJSON) => {
          if (resJSON.success) {
            setDropDownOps(resJSON.categories.map((cate) => cate.name));
          }
        });
      });
    }
  }, [field]);

  return (
    <div className="w-full shadow" ref={ref}>
      <div
        className="px-4 py-1 w-full bg-white flex items-center text-charcoal-grey font-medium justify-between cursor-pointer select-min-height"
        type="button"
        onClick={() => setOpenDownDown(!openDropDown)}
      >
        <div className="flex flex-wrap">
          {selectedOps.map((op) => {
            return (
              <div
                className="h-8 flex items-center justify-between bg-white-two px-2 rounded m-1 border"
                style={{ borderColor: op }}
                onClick={(e) => e.stopPropagation()}
                key={op}
              >
                <p className="mr-4 text-charcoal-grey font-medium uppercase">
                  {op}
                </p>
                <button
                  className="z-10"
                  type="button"
                  onClick={(e) => {
                    setSelectedOps(selectedOps.filter((ddOp) => ddOp !== op));
                    setDropDownOps([...dropDownOps, op]);
                  }}
                >
                  <CloseIcon />
                </button>
              </div>
            );
          })}
        </div>
        <DropDownIcon />
      </div>
      <div className="relative w-full">
        <div
          className={classnames(
            "absolute w-full top-sm bg-white shadow px-4 z-20 select-min-height ",
            openDropDown ? "block" : "hidden"
          )}
        >
          <div className="flex flex-wrap">
            {dropDownOps.map((op) => {
              return (
                <button
                  className="mx-4 h-8 px-8 shadow border my-1 text-charcoal-grey font-medium uppercase hover:text-pale-orange rounded shadow"
                  type="button"
                  key={op}
                  style={{ borderColor: op }}
                  onClick={() => {
                    setSelectedOps([...selectedOps, op]);
                    setDropDownOps(dropDownOps.filter((ddOp) => ddOp !== op));
                  }}
                >
                  {op}
                </button>
              );
            })}
          </div>
          {enableAddNew ? (
            <div className="flex p-4 items-center">
              <button onClick={addNewOp}>
                <AddIcon />
              </button>
              <input
                className="px-4 py-2"
                placeholder="Add new category"
                value={newOp}
                onChange={(e) => setNewOp(e.target.value)}
              />
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  );
};
