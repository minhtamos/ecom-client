import React, { useState, useRef } from "react";
import {useHistory} from "react-router-dom";
import classnames from "classnames";
import Swal2 from "sweetalert2";
import { DropDownIcon, EditIcon, TrashIcon } from "../icons";
import { useOnClickOutSide } from "../../hooks";
import { getDateFormated } from "../../utils";

export const AdminProductsRow = ({ product, getData }) => {
  const [openEdit, setOpenEdit] = useState(false);
  const ref = useRef();
  const history = useHistory();

  const getSoldCell = () => {
    console.log(product)
    const { quantities, solds } = product;
    const totalQ = quantities.reduce((pre, cur) => {
      return pre + cur;
    });
    const totalS = solds.reduce((pre, cur) => {
      return pre + cur;
    });
    return `${totalS} / ${totalQ}`;
  };

  const getProfitCell = () => {
    const { solds, price } = product;
    const totalS = solds.reduce((pre, cur) => {
      return pre + cur;
    });
    return (totalS * price).toFixed(2);
  };

  const deleteProduct = async () => {
    const { friendlyUrl } = product;
    const res = await fetch(`/api/product?url=${friendlyUrl}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const resJSON = await res.json();
    if (resJSON.success) {
      getData();
    } else {
      Swal2.fire({
        icon: "error",
        title: "Can not delete product!",
        text: resJSON.error.name,
      });
    }
  };

  useOnClickOutSide(ref, () => {
    setOpenEdit(false);
  });

  return (
    <tr>
      <td>
        <div className="flex">
          <img src={product.images[0]} className="w-10 h-12" alt="thump" />
          <div className="ml-4">
            <h5 className="text-charcoal-grey text-sm font-medium">
              {product.name}
            </h5>
            <p className="text-greyish">Women, Casual dresses</p>
          </div>
        </div>
      </td>
      <td className="text-center text-charcoal-grey font-medium text-sm">
        {getSoldCell()}
      </td>
      <td className="text-center text-charcoal-grey font-medium text-sm">
        {getDateFormated(product.createdAt)}
      </td>
      <td className="text-center text-charcoal-grey font-medium text-sm">
        {getProfitCell()}
      </td>
      <td className="text-center text-charcoal-grey font-medium text-sm">
        <div className="relative" ref={ref}>
          <button
            className="flex items-center text-charcoal-grey font-semibold"
            onClick={() => setOpenEdit(!openEdit)}
          >
            Action <DropDownIcon />
          </button>
          <div
            className={classnames(
              "absolute top-nav w-32 z-10",
              openEdit ? "block" : "hidden"
            )}
          >
            <button className="flex items-center bg-white text-charcoal-grey font-medium py-2 shadow w-32 px-2 hover:bg-white-three" onClick={() => history.push(`/admin/products/edit/${product.friendlyUrl}`)} >
              <EditIcon />
              <span className="ml-2">Edit</span>
            </button>
            <button
              className="flex items-center bg-white text-charcoal-grey font-medium py-2 shadow w-32 px-2 hover:bg-white-three"
              onClick={deleteProduct}
            >
              <TrashIcon />
              <span className="ml-2">Remove</span>
            </button>
          </div>
        </div>
      </td>
    </tr>
  );
};
