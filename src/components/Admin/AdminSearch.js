import React from "react";
import { AdminSearchIcon } from "../icons";

export const AdminSearch = ({ placeholder, search, setSearch, onClick }) => {
  return (
    <div className="bg-white border border-white-three px-4 py-3 flex items-center">
      <button onClick={onClick}>
        <AdminSearchIcon />
      </button>
      <input
        className="ml-2 w-56"
        placeholder={placeholder}
        value={search}
        onChange={(e) => setSearch(e.target.value)}
      />
    </div>
  );
};
