import React, { useState, useEffect } from "react";
import { useQuery } from "../../hooks";
import { ExportIcon, DropDownIcon } from "../icons";
import { AdminSearch } from "./AdminSearch";
import { AdminOrdersRow } from "./AdminOrdersRow";
import { AdminPagination } from "./AdminPagination";
import { useHistory } from "react-router";

export const AdminOrders = () => {
  const history = useHistory();
  const [orders, setOrders] = useState([]);
  const [maxPage, setMaxPage] = useState(1);
  const [total, setTotal] = useState(1);
  const [search, setSearch] = useState("");
  const [sortby, setSortby] = useState("");
  const query = useQuery();
  const page = parseInt(query.get("page")) || 1;

  const getData = () => {
    fetch(`/api/order/pagination/table?page=${page}&search=${search}&sortby=${sortby}`).then(
      (res) =>
        res.json().then((resJSON) => {
          if (resJSON.success) {
            if (resJSON.pages < page) {
              history.replace(`/admin/products?page=${resJSON.pages}`);
            }
            if (resJSON.orders.length > 0) {
              setOrders(resJSON.orders);
            }
            if (maxPage !== resJSON.pages) {
              setMaxPage(resJSON.pages);
            }
            if (total !== resJSON.total) {
              setTotal(resJSON.total);
            }
          } else {
            console.log(resJSON);
          }
        })
    );
  };

  const toggleStatus = () => {
    sortby === "asc-status" ? setSortby("desc-status") : setSortby("asc-status")
    getData();
  }

  useEffect(() => {
    if (page < 1) {
      history.replace(`/admin/orders?page=${1}&search=${search}`);
    } else {
      getData();
    }
  }, [page]); //eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className="w-full">
      <div className="flex justify-between mt-4">
        <div className="flex items-center">
          <h3 className="font-semibold text-greyish text-xs">SORT BY</h3>
          <div className="bg-white flex justify-center items-center ml-4">
            <select className="block py-3 px-4 appearance-none bg-white text-charcoal-grey flex justify-between focus:outline-none">
              <option>Date added</option>
              <option>Apples</option>
              <option>Bananas</option>
              <option>Grapes</option>
              <option>Oranges</option>
            </select>
            <DropDownIcon />
          </div>
        </div>
        <div className="flex items-center">
          <AdminSearch
            placeholder="Search by order id"
            search={search}
            setSearch={setSearch}
            onClick={getData}
          />
          <button className="px-4 py-3 text-pale-orange bg-white flex items-center rounded-sm font-semibold ml-4">
            <ExportIcon /> <span className="ml-2">Export</span>
          </button>
        </div>
      </div>
      <div className="w-full font-light text-greyish text-xs mt-8 bg-white shadow-lg inline-block px-4">
        <table className="w-full admin-table">
          <thead>
            <tr className="border-b border-white-three">
              <th>ORDER ID</th>
              <th>ORDER DATE</th>
              <th>DETAIL</th>
              <th>TOTAL ($)</th>
              <th>
                <button className="font-bold flex items-center" onClick={toggleStatus}>
                  STATUS <DropDownIcon />
                </button>
              </th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {orders.length === 0 ? (
              <tr className="">
                <td colSpan={5} className="text-strawberry italic">
                  No records
                </td>
              </tr>
            ) : (
              orders.map((order) => {
                return (
                  <AdminOrdersRow
                    order={order}
                    key={order._id}
                    getData={getData}
                  />
                );
              })
            )}
          </tbody>
        </table>
        <div className="mt-16 flex py-4 justify-between">
          <p className="text-charcoal-grey font-medium">
            {`Show ${(page - 1) * 10 + 1} to ${
              page * 10 > total ? total : page * 10
            } of ${total} entries`}
          </p>
          <AdminPagination curPage={page} maxPage={maxPage} nav="orders" />
        </div>
      </div>
    </div>
  );
};
