import React, { useState } from "react";
import Swal2 from "sweetalert2";
import { AdminMulSelect } from "./AdminMulSelect";
import { cateBanners } from "../../constants";

export const AdminCategoryRow = ({ category }) => {
  const [selectedCharacters, setCharacters] = useState(category.characters);

  const updateCharacters = () => {
    const body = {
      id: category._id,
      characters: selectedCharacters,
    };
    fetch("/api/category/characters", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    }).then((res) =>
      res.json().then((resJSON) => {
        if (resJSON.success) {
          Swal2.fire("Updated");
        }
      })
    );
  };

  return (
    <tr key={category.name}>
      <td>{category.name}</td>
      <td className="w-2/3">
        <AdminMulSelect
          options={cateBanners.map((cate) => cate.title)}
          selectedOps={selectedCharacters}
          setSelectedOps={setCharacters}
        />
      </td>
      <td>
        <button
          className=" bg-pale-orange text-white font-medium px-4 py-2 hover:bg-orange-400"
          onClick={updateCharacters}
        >
          Update
        </button>
      </td>
    </tr>
  );
};
