import React, { useState } from "react";
import { CloseIcon } from "../icons";

export const AdminMulInput = ({ selectedOps, setSelectedOps }) => {
  const [input, setInput] = useState("");
  return (
    <div className="shadow">
      <div
        className="px-4 py-1 w-full bg-white flex items-center text-charcoal-grey font-medium justify-between cursor-pointer select-min-height"
        type="button"
      >
        <div className="flex flex-wrap">
          {selectedOps.map((op, index) => {
            return (
              <div
                className="h-8 flex items-center justify-between bg-white-two px-2 rounded m-1"
                onClick={(e) => e.stopPropagation()}
                key={`input-${index}`}
              >
                <p className="mr-4 text-charcoal-grey font-medium">{op}</p>
                <button
                  className="z-10"
                  type="button"
                  onClick={() =>
                    setSelectedOps(selectedOps.filter((op, i) => i !== index))
                  }
                >
                  <CloseIcon />
                </button>
              </div>
            );
          })}
        </div>
      </div>
      <input
        className="h-10 w-full px-4 text-charcoal-grey font-medium"
        type="number"
        value={input}
        onChange={(e) => setInput(e.target.value)}
        placeholder="input one quantity then press enter"
        onKeyPress={(e) => {
          if (e.key === "Enter" && input !== "") {
            setSelectedOps([...selectedOps, input]);
            setInput("");
          }
        }}
      />
    </div>
  );
};
