import React, { useState, useRef } from "react";
import classnames from "classnames";
import { DropDownIcon } from "../icons";
import { useOnClickOutSide } from "../../hooks";
import "./AdminMulSelect.css";

export const AdminSelect = ({ options, selectedOp, setSelectedOp }) => {
  const [openDropDown, setOpenDownDown] = useState(false);
  const ref = useRef();
  useOnClickOutSide(ref, () => {
    setOpenDownDown(false);
  });

  return (
    <div className="w-full shadow" ref={ref}>
      <div
        className="px-4 py-1 w-full bg-white flex items-center text-charcoal-grey font-medium justify-between cursor-pointer select-min-height"
        type="button"
        onClick={() => setOpenDownDown(!openDropDown)}
      >
        <div className="flex flex-wrap">{selectedOp}</div>
        <DropDownIcon />
      </div>
      <div className="relative w-full">
        <div
          className={classnames(
            "absolute w-full top-sm bg-white shadow px-4 z-20 select-min-height select-min-width flex-wrap items-center",
            openDropDown ? "flex" : "hidden"
          )}
        >
          {options.map((op) => {
            return (
              <button
                className="w-1/2 mx-4 h-8 px-2 bg-white-two hover:bg-white-three shadow border border-white-two my-1"
                type="button"
                key={op}
                onClick={() => {
                  setSelectedOp(op);
                  setOpenDownDown(false);
                }}
              >
                {op}
              </button>
            );
          })}
        </div>
      </div>
    </div>
  );
};
