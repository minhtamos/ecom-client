import React, { useState, useRef } from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";
import { useDispatch } from "react-redux";
import { userActionType } from "../../stores/user/userActionType";
import { DropDownIcon, MailIcon, BellIcon } from "../icons";
import { useOnClickOutSide } from "../../hooks";

export const AdminHeader = ({ title }) => {
  const [openAccountOp, setOpenAccountOp] = useState(false);
  const dispatch = useDispatch();
  const ref = useRef();

  useOnClickOutSide(ref, () => setOpenAccountOp(false));

  return (
    <header className="flex justify-between w-full items-center">
      <h1 className="text-2xl font-bold text-dark-grey">
        {title ? title.charAt(0).toUpperCase() + title.slice(1) : ""}
      </h1>
      <div className="flex items-center" ref={ref}>
        <img
          alt="avatar"
          src="https://c7.uihere.com/files/340/946/334/avatar-user-computer-icons-software-developer-avatar.jpg"
          className="w-10 h-10 object-cover rounded-full"
        />
        <div className="relative z-20">
          <button
            className="flex items-center text-dark-grey font-bold mx-4"
            onClick={() => setOpenAccountOp(!openAccountOp)}
          >
            Lucile Bush <DropDownIcon />
          </button>
          <div
            className={classnames(
              "absolute top-nav right-0 shadow-md bg-white w-48 text-xs text-greyish-brown",
              openAccountOp ? "block" : "hidden"
            )}
          >
            <Link
              to="/account/setting"
              className="p-2 border-b hover:bg-gray-200 block"
              onClick={() => setOpenAccountOp(false)}
            >
              <p className="whitespace-no-wrap">Account setting</p>
            </Link>
            <button
              className="p-2 hover:bg-gray-200 w-full flex flex-start focus:outline-none"
              onClick={() => {
                localStorage.removeItem("authToken");
                dispatch({ type: userActionType.logOut });
                setOpenAccountOp(false);
              }}
            >
              Logout
            </button>
          </div>
        </div>
        <div className="mx-4 relative">
          <MailIcon />
          <div className="absolute top-badge right-badge text-2xs rounded-full bg-strawberry text-white font-bold w-5 h-4 flex justify-center items-center">
            9+
          </div>
        </div>
        <div className="ml-2 relative">
          <BellIcon />
          <div className="absolute top-badge right-badge text-2xs rounded-full bg-strawberry text-white font-bold w-5 h-4 flex justify-center items-center">
            9+
          </div>
        </div>
      </div>
    </header>
  );
};
