import React, { useRef, useState } from "react";
import classnames from "classnames";
import { DropDownIcon } from "../icons";
import { useOnClickOutSide } from "../../hooks";
import { getDateFormated } from "../../utils";
import Swal2 from "sweetalert2";

export const AdminOrdersRow = ({ order, getData }) => {
  const [openEdit, setOpenEdit] = useState(false);
  const ref = useRef();
  useOnClickOutSide(ref, () => {
    setOpenEdit(false);
  });

  const getDetail = () => {
    return `${order.name} (${order.size.toUpperCase()}) x ${order.quantity}`;
  };

  const getTotal = () => {
    return order.price * order.quantity;
  };

  const markAsCompleted = async () => {
    const res = await fetch(`/api/order/status/completed?id=${order._id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const resJSON = await res.json();
    if (!resJSON.success) {
      Swal2.fire({
        title: "Can not mark as completed",
        text: resJSON.error.name,
      });
    } else {
      getData();
    }
    setOpenEdit(false);
  };

  const markAsCanceled = async () => {
    const res = await fetch(`/api/order/status/canceled?id=${order._id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const resJSON = await res.json();
    if (!resJSON.success) {
      Swal2.fire({
        title: "Can not mark as completed",
        text: resJSON.error.name,
      });
    } else {
      getData();
    }
    setOpenEdit(false);
  };

  const markAsPending = async () => {
    const res = await fetch(`/api/order/status/pending?id=${order._id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const resJSON = await res.json();
    if (!resJSON.success) {
      Swal2.fire({
        title: "Can not mark as completed",
        text: resJSON.error.name,
      });
    } else {
      getData();
    }
    setOpenEdit(false);
  };

  return (
    <tr>
      <td className="text-center text-charcoal-grey font-medium text-xs">
        {order._id}
      </td>
      <td className="text-center text-charcoal-grey font-medium text-sm">
        {getDateFormated(order.orderedAt)}
      </td>
      <td className="text-center text-charcoal-grey font-medium text-sm">
        {getDetail()}
      </td>
      <td className="text-center text-charcoal-grey font-medium text-sm">
        {getTotal()}
      </td>
      <td className="text-center text-charcoal-grey font-medium text-sm">
        <div
          className={classnames(
            "text-white rounded-full bg-pea-green text-xss w-20 h-5 flex justify-center items-center",
            {
              "bg-pending": order.status === "pending",
              "bg-pea-green": order.status === "completed",
              "bg-cancel": order.status === "canceled",
            }
          )}
        >
          {order
            ? order.status.charAt(0).toUpperCase() + order.status.slice(1)
            : "not string"}
        </div>
      </td>
      <td>
        <div className="relative" ref={ref}>
          <button
            className="flex items-center text-charcoal-grey font-semibold"
            onClick={() => setOpenEdit(!openEdit)}
          >
            Action <DropDownIcon />
          </button>
          <div
            className={classnames(
              "absolute top-nav w-40 z-10 right-0 flex-col",
              openEdit ? "flex" : "hidden"
            )}
          >
            <button
              className="flex items-center bg-white text-charcoal-grey font-medium py-3 pl-4 shadow px-2 hover:bg-white-three"
              onClick={markAsCompleted}
            >
              <div className="w-2 h-2 rounded-full bg-pea-green"></div>
              <span className="ml-2 font-medium">Mark as Completed</span>
            </button>
            <button
              className="flex items-center bg-white text-charcoal-grey font-medium py-3 pl-4 shadow px-2 hover:bg-white-three"
              onClick={markAsCanceled}
            >
              <div className="w-2 h-2 rounded-full bg-cancel"></div>
              <span className="ml-2 font-medium">Mark as Canceled</span>
            </button>
            <button
              className="flex items-center bg-white text-charcoal-grey font-medium py-3 pl-4 shadow px-2 hover:bg-white-three"
              onClick={markAsPending}
            >
              <div className="w-2 h-2 rounded-full bg-pending"></div>
              <span className="ml-2 font-medium">Mark as Pending</span>
            </button>
          </div>
        </div>
      </td>
    </tr>
  );
};
