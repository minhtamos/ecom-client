import React, { useState, useEffect } from "react";
import { AdminCategoryRow } from "./AdminCategoryRow";

export const AdminCategory = () => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    fetch("/api/category").then((res) =>
      res.json().then((resJSON) => {
        if (resJSON.success) {
          setCategories(resJSON.categories);
        }
      })
    );
  }, []);

  return (
    <div>
      <table className="admin-table w-full text-greyish">
        <thead>
          <tr className="border-b border-white-three">
            <th>Name</th>
            <th>Characters</th>
            <th>Update characters</th>
          </tr>
        </thead>
        <tbody className="text-charcoal-grey">
          {categories.map((cate) => {
            return <AdminCategoryRow category={cate} key={cate.name} />;
          })}
        </tbody>
      </table>
    </div>
  );
};
