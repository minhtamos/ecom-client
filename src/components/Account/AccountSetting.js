import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import classnames from "classnames";
import Swal2 from "sweetalert2";
import { emailReg } from "../../constants";
import { userActionType } from "../../stores/user/userActionType";
import { Input } from "../form";
// import "./AccountSetting.css";

export const AccountSetting = () => {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [name, setName] = useState(user.name);
  const [email, setEmail] = useState(user.email);
  const [openEdit, setOpenEdit] = useState(false);
  const [nameError, setNameError] = useState({ isError: false, msg: "" });
  const [emailError, setEmailError] = useState({ isError: false, msg: "" });

  const resetError = () => {
    setNameError({ isError: false, msg: "" });
    setEmailError({ isError: false, msg: "" });
  };

  const validated = () => {
    resetError();
    let result = true;
    //check name
    if (name.length === 0) {
      setNameError({
        isError: true,
        msg: "Please enter a valid name!",
      });
      result = false;
    }
    //check email
    if (!emailReg.test(String(email).toLowerCase())) {
      setEmailError({
        isError: true,
        msg: "Please enter a valid name!",
      });
      result = false;
    }
    return result;
  };

  const changeInfo = async (e) => {
    e.preventDefault();
    if (validated()) {
      const body = { email, name, oldEmail: user.email };
      const res = await fetch("/api/user/info", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
      const resJSON = await res.json();
      if (resJSON.success) {
        Swal2.fire("Change info successful!");
        dispatch({ type: userActionType.changeInfo, payload: { name, email } });
      } else {
        Swal2.fire({
          icon: "error",
          title: "Can not change info!",
          text: resJSON.error.name || "Something wrong with server",
        });
      }
    }
  };

  return (
    <div className="ml-16 text-dark-grey">
      <div className="w-account-form flex justify-between">
        <h2 className="font-bold">Infomation</h2>
        {openEdit ? (
          ""
        ) : (
          <button
            className="text-greyish-brown font-medium"
            onClick={() => setOpenEdit(true)}
          >
            Edit
          </button>
        )}
      </div>
      {user.authenticated ? (
        <form
          className="px-8 py-8 bg-f9 mt-4 w-account-form"
          onSubmit={changeInfo}
        >
          {openEdit ? (
            <Input
              title="NAME"
              type="text"
              placeholder="Enter your name..."
              value={name}
              onChange={(e) => setName(e.target.value)}
              error={nameError}
              bgClass="bg-white"
              borderClass="broder-none"
            />
          ) : (
            <div>
              <h5 className="text-dark-grey font-medium">NAME</h5>
              <p className="text-greyish-brown">{user.name}</p>
            </div>
          )}
          {openEdit ? (
            <Input
              title="EMAIL"
              type="email"
              placeholder="Enter your email..."
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              error={emailError}
              bgClass="bg-white"
              borderClass="broder-none"
            />
          ) : (
            <div className="mt-8">
              <h5 className="text-dark-grey font-medium">EMAIL</h5>
              <p className="text-greyish-brown">{user.email}</p>
            </div>
          )}
          <div className="flex justify-end mt-8">
            <div>
              <button
                className="font-medium"
                type="button"
                onClick={() => setOpenEdit(false)}
              >
                Cancel
              </button>
              <button
                className={classnames(
                  "ml-4 px-8 py-2",
                  openEdit
                    ? "text-white bg-orange-400 hover:bg-orange-500"
                    : "bg-white-four text-white"
                )}
                disabled={!openEdit}
              >
                Save
              </button>
            </div>
          </div>
        </form>
      ) : (
        <h3 className="text-center text-red-600 italic mt-8">
          Please login to change your info
        </h3>
      )}
    </div>
  );
};
