import { AccountSetting } from "./AccountSetting";
import { AccountChangePassword } from "./AccountChangePassword";

export { AccountSetting, AccountChangePassword };
