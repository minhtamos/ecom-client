import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Swal2 from "sweetalert2";
import { userActionType } from "../../stores/user/userActionType";
import { Input } from "../form";

export const AccountChangePassword = () => {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [curPass, setCurPasss] = useState("");
  const [newPass, setNewPass] = useState("");
  const [reNewPass, setReNewPass] = useState("");
  const [curPassError, setCurPasssError] = useState({
    isError: false,
    msg: "",
  });
  const [newPassError, setNewPassError] = useState({ isError: false, msg: "" });
  const [reNewPassError, setReNewPassError] = useState({
    isError: false,
    msg: "",
  });

  const resetError = () => {
    setNewPassError({ isError: false, msg: "" });
    setReNewPassError({ isError: false, msg: "" });
  };

  const validated = () => {
    resetError();
    let result = true;
    if (newPass.length < 6) {
      setNewPassError({
        isError: true,
        msg: "Your passwords must be more than 6 characters!",
      });
      result = false;
    }
    if (newPass !== reNewPass) {
      setReNewPassError({
        isError: true,
        msg: "Your re-enter new password is not the same as new password",
      });
      result = false;
    }
    if (curPass !== user.password) {
      setCurPasssError({
        isError: true,
        msg: "Wrong current password",
      });
      result = false;
    }
    return result;
  };

  const changePassword = async (e) => {
    e.preventDefault();
    const body = { curPass, newPass, email: user.email };
    if (validated()) {
      const res = await fetch("/api/user/password", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
      const resJSON = await res.json();
      if (resJSON.success) {
        Swal2.fire("Change password successful!");
        dispatch({
          type: userActionType.changePass,
          payload: { password: newPass },
        });
      } else {
        Swal2.fire({
          icon: "error",
          title: "Can not change password!",
          text: resJSON.error.name || "Something wrong with server",
        });
      }
    }
  };

  return (
    <div className="ml-16 text-dark-grey">
      <h2 className="font-bold">Change password</h2>
      {user.authenticated ? (
        <form
          className="px-8 py-8 bg-f9 mt-4 w-account-form"
          onSubmit={changePassword}
        >
          <Input
            title="CURRENT PASSWORD"
            type="password"
            placeholder="Enter your password..."
            value={curPass}
            onChange={(e) => setCurPasss(e.target.value)}
            error={curPassError}
            bgClass="bg-white"
            borderClass="broder-none"
          />
          <Input
            title="NEW PASSWORD"
            type="password"
            placeholder="Enter your password..."
            value={newPass}
            onChange={(e) => setNewPass(e.target.value)}
            error={newPassError}
            bgClass="bg-white"
            borderClass="broder-none"
          />
          <Input
            title="RE-ENTER PASSWORD"
            type="password"
            placeholder="Enter your password..."
            value={reNewPass}
            onChange={(e) => setReNewPass(e.target.value)}
            error={reNewPassError}
            bgClass="bg-white"
            borderClass="broder-none"
          />
          <div className="flex justify-end mt-8">
            <div>
              <button className="font-medium">Cancel</button>
              <button className="ml-4 px-8 py-2 text-white bg-orange-400 hover:bg-orange-500">
                Save
              </button>
            </div>
          </div>
        </form>
      ) : (
        <h3 className="text-center text-red-600 italic">
          Please login to change your password
        </h3>
      )}
    </div>
  );
};
