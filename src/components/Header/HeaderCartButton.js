import React, { useState, useRef } from "react";
import { CartIcon } from "../icons";
import { HeaderCartDropDown } from "./HeaderCartDropDown";
import { useOnClickOutSide } from "../../hooks";
import { useSelector } from "react-redux";

export const HeaderCartButton = () => {
  const [show, setShow] = useState(false);
  const ref = useRef();
  const cartNum = useSelector((state) => state.cart).length;

  useOnClickOutSide(ref, () => {
    setShow(false);
  });

  return (
    <div className="ml-4" ref={ref}>
      <button
        className="relative focus:outline-none"
        onClick={() => setShow(!show)}
      >
        <div className="absolute rounded-full w-4 h-4 flex justify-center items-center text-xs text-white bg-pale-orange top-badge right-badge">
          {cartNum}
        </div>
        <CartIcon />
      </button>
      <HeaderCartDropDown show={show} setShow={setShow} />
    </div>
  );
};
