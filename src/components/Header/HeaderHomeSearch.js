import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { SearchIcon } from "../icons";

export const HeaderHomeSearch = () => {
  const history = useHistory();
  const [search, setSearch] = useState("");
  return (
    <div
      className="border rounded-full px-2 py-1 flex items-center border-greyish-two hover:border-black"
    >
      <input
        type="text"
        className="focus:outline-none text-xs"
        placeholder="Search"
        value={search}
        onChange={(e) => setSearch(e.target.value)}
      />
      <button onClick={() => history.push(`/list?search=${search}`)}>
        <SearchIcon />
      </button>
    </div>
  );
};
