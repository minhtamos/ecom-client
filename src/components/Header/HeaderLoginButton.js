import React, { useState } from "react";
import Modal from "react-modal";
import { HeaderLoginModal } from "./HeaderLoginModal";
import { customModal } from "../../constants";

export const HeaderLoginButton = () => {
  const [modalIsOpen, setIsOpen] = useState(false);
  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }
  return (
    <>
      <button
        className="py-1 px-8 border rounded-full font-bold ml-4 focus:outline-none border-pale-orange text-pale-orange hover:bg-orange-500 hover:text-white"
        onClick={openModal}
      >
        Log In
      </button>

      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customModal}
      >
        <HeaderLoginModal />
      </Modal>
    </>
  );
};
