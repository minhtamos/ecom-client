import React, { useState, useRef } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import classnames from "classnames";
import { useDispatch } from "react-redux";
import { useOnClickOutSide } from "../../hooks";
import { userActionType } from "../../stores/user/userActionType";
import { HeaderRegisterButton } from "./HeaderRegisterButton";
import { HeaderHomeSearch } from "./HeaderHomeSearch";
import { HeaderLoginButton } from "./HeaderLoginButton";
import { HeaderCartButton } from "./HeaderCartButton";
import { LogoIcon } from "../icons/LogoIcon";
import "./Header.css"

export const Header = () => {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [openAccountOp, setOpenAccountOp] = useState(false);
  const ref = useRef();

  useOnClickOutSide(ref, () => {
    setOpenAccountOp(false);
  });

  const HeaderStyles = {
    avatar: {
      width: "32px",
      height: "32px",
      objectFit: "cover",
    },
    accOp: {
      borderColor: "#eaeaea",
    },
  };

  return (
    <header className="px-16 py-3 flex justify-between items-center">
      <HeaderHomeSearch />
      <a href="/">
        <LogoIcon />
      </a>
      <div className="flex items-center">
        {user.authenticated ? (
          <div ref={ref}>
            <button
              className="focus:outline-none rounded-full border-2 border-pumpkin-orange overflow-hidden"
              onClick={() => setOpenAccountOp(!openAccountOp)}
            >
              <img
                src="https://c7.uihere.com/files/340/946/334/avatar-user-computer-icons-software-developer-avatar.jpg"
                style={HeaderStyles.avatar}
                alt="ava"
              />
            </button>
            <div className="relative z-20">
              <div
                className={classnames(
                  "absolute top-cart right-0 shadow-md bg-white w-48 text-xs text-greyish-brown",
                  openAccountOp ? "block" : "hidden"
                )}
              >
                <Link
                  to="/account/setting"
                  className="p-2 border-b hover:bg-gray-200 block"
                  style={HeaderStyles.accOp}
                  onClick={() => setOpenAccountOp(false)}
                >
                  <p className="whitespace-no-wrap">Account setting</p>
                </Link>
                <button
                  className="p-2 hover:bg-gray-200 w-full flex flex-start focus:outline-none"
                  style={HeaderStyles.accOp}
                  onClick={() => {
                    localStorage.removeItem("authToken");
                    dispatch({ type: userActionType.logOut });
                    setOpenAccountOp(false);
                  }}
                >
                  Logout
                </button>
              </div>
            </div>
          </div>
        ) : (
          <div>
            <HeaderRegisterButton />
            <HeaderLoginButton />
          </div>
        )}
        <HeaderCartButton />
      </div>
    </header>
  );
};
