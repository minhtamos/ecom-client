import React, { useState } from "react";
import { Input, FormTitle, FormBtn } from "../form";
import { Link } from "react-router-dom";
import { emailReg } from "../../constants";

export const HeaderRegisterModal = () => {
  //input state
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  //error state
  const [nameError, setNameError] = useState({ isError: false, msg: "" });
  const [emailError, setEmailError] = useState({ isError: false, msg: "" });
  const [passwordError, setPasswordError] = useState({
    isError: false,
    msg: "Your passwords must be more than 6 characters.",
  });
  //res_errors
  const register_errors = {
    userExisted: "user existed",
  };
  //if success then show success message
  const [isSuccess, setIsSuccess] = useState(false);

  const reset = () => {
    setNameError({ isError: false, msg: "" });
    setEmailError({ isError: false, msg: "" });
    setPasswordError({
      isError: false,
      msg: "Your passwords must be more than 6 characters.",
    });
    setIsSuccess(false);
  };

  const validate = () => {
    let validated = true;
    //check name
    if (name.length === 0) {
      setNameError({
        isError: true,
        msg: "Please enter a valid name!",
      });
      validated = false;
    }
    //check password
    if (password.length < 6) {
      setPasswordError({
        isError: true,
        msg: "Your passwords must be more than 6 characters!",
      });
      validated = false;
    }
    //check email
    if (!emailReg.test(String(email).toLowerCase())) {
      setEmailError({
        isError: true,
        msg: "Please enter a valid name!",
      });
      validated = false;
    }
    return validated;
  };

  const register = async (e) => {
    e.preventDefault();
    reset();
    if (validate()) {
      const body = {
        name,
        email,
        password,
      };
      const res = await fetch("/api/user/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
      const resJSON = await res.json();
      if (resJSON.success === true) {
        setIsSuccess(true);
      } else {
        if (resJSON.error.name === register_errors.userExisted) {
          setEmailError({
            isError: true,
            msg: "Email has been taken!",
          });
        }
      }
    }
  };

  return (
    <div className="flex flex-col justify-between w-modal">
      <form className="px-12 py-4" onSubmit={register}>
        <FormTitle title="Register" />
        <Input
          title="NAME"
          type="text"
          placeholder="Enter your name..."
          value={name}
          onChange={(e) => setName(e.target.value)}
          error={nameError}
        />
        <Input
          title="E-MAIL"
          type="email"
          placeholder="Enter your email..."
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          error={emailError}
        />
        <Input
          title="PASSWORD"
          type="password"
          placeholder="Enter your password..."
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          error={passwordError}
        />
        <p className="text-xs text-center mt-4">
          By creating an account you agree to the <br />{" "}
          <span className="text-pumpkin-orange">Terms of Service</span> and{" "}
          <span className="text-pumpkin-orange">Privacy Policy</span>
        </p>
        <FormBtn title="Register" />
        <p className="text-center mt-2 text-xs text-green-600 font-bold min-h-text">
          {isSuccess ? "Create your new account successful!" : ""}
        </p>
      </form>
      <div className="flex justify-center items-center py-4 border-t border-gray-400">
        <p className="text-xs text-center">
          Do you have an account?{" "}
          <Link
            to="/"
            className="underline text-pumpkin-orange underline-margin"
          >
            Log In
          </Link>
        </p>
      </div>
    </div>
  );
};
