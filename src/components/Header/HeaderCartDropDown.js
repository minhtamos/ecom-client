import React from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import classnames from "classnames";
import { CrossIcon } from "../icons/CrossIcon";
import { cartActionType } from "../../stores/cart/cartActionType";

export const HeaderCartDropDown = ({ show, setShow }) => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);

  const deleteItemInCart = (index) => {
    dispatch({ type: cartActionType.deleteIndex, payload: index });
  };

  return (
    <div className="relative">
      <div
        className={classnames(
          "absolute right-0 top-cart z-20 bg-fb shadow-lg w-72",
          show ? "block" : "hidden"
        )}
      >
        <div className="cart-max-height overflow-scroll">
          {cart.length === 0 ? (
            <div className="text-xs flex justify-center items-center p-4 border-b border-ec">
              Cart is empty
            </div>
          ) : (
            cart.map((item, index) => {
              return (
                <div
                  className="flex p-3 text-xs border-b border-ec"
                  key={`${item.name}-${item.size}-${item.color}`}
                >
                  <img
                    src={item.image}
                    className="mr-4 h-cart-image w-cart-image object-contain"
                    alt="cart thump"
                  />
                  <div className="flex flex-col justify-between w-full ml-2 my-1">
                    <div className="flex justify-between">
                      <h3 className="font-semibold">{item.name}</h3>
                      <button
                        onClick={() => deleteItemInCart(index)}
                        className="focus:outline-none"
                      >
                        <CrossIcon />
                      </button>
                    </div>
                    <div className="flex justify-between">
                      <p className="text-greyish-brown">${item.price}</p>
                      <p className="text-dark-grey opacity-40">
                        {`${item.size.toUpperCase()} • ${item.color} • ${
                          item.quantity
                        } pcs`}
                      </p>
                    </div>
                  </div>
                </div>
              );
            })
          )}
        </div>
        <div className="py-4 flex justify-center items-center font-bold text-pale-orange">
          <Link to="/cart" onClick={() => setShow(false)}>
            View cart
          </Link>
        </div>
      </div>
    </div>
  );
};
