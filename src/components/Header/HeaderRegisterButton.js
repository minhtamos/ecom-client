import React, { useState } from "react";
import Modal from "react-modal";
import { HeaderRegisterModal } from "./HeaderRegisterModal";
import { customModal } from "../../constants";

export const HeaderRegisterButton = () => {
  const [modalIsOpen, setIsOpen] = useState(false);
  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }
  return (
    <>
      <button
        onClick={openModal}
        className="focus:outline-none text-greyish-brown hover:text-black"
      >
        Register
      </button>

      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customModal}
      >
        <HeaderRegisterModal />
      </Modal>
    </>
  );
};
