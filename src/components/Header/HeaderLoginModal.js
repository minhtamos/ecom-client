import React, { useState } from "react";
import { Input, FormTitle, FormBtn, CheckBox } from "../form";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { userActionType } from "../../stores/user/userActionType";

export const HeaderLoginModal = () => {
  //input state
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [checkRePass, setCheckRePass] = useState(false);
  //if error, show msg and red the input
  const [isErr, setIsErr] = useState(false);
  //login errors
  const loginErrors = {
    wrongEmailPass: "wrong email or password",
  };
  //redux
  const dispatch = useDispatch();

  //when press login btn
  const login = async (e) => {
    e.preventDefault();
    const body = {
      email,
      password,
    };
    const res = await fetch("/api/user/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
    const resJSON = await res.json();
    if (resJSON.success) {
      const { user } = resJSON;
      dispatch({
        type: userActionType.logIn,
        payload: { ...user, authenticated: true },
      });
    } else {
      if (resJSON.error.name === loginErrors.wrongEmailPass) {
        setIsErr(true);
      }
    }
  };

  return (
    <div className="flex flex-col justify-between w-modal z-20">
      <form className="px-12 py-4" onSubmit={login}>
        <FormTitle title="Log In" />
        <p className="text-center text-strawberry min-h-text text-xs block">
          {isErr ? "Your e-mail/password is invalid!" : ""}
        </p>
        <Input
          title="E-MAIL"
          type="email"
          error={{ isError: false }}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="Enter your email..."
        />
        <Input
          title="PASSWORD"
          type="password"
          error={{ isError: false }}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Enter your password..."
        />
        <div className="flex text-xs justify-between">
          <CheckBox
            title="Remember password"
            checked={checkRePass}
            toggle={setCheckRePass}
          />
          <p className="font-medium">Forgot your password?</p>
        </div>
        <FormBtn title="Log In" />
      </form>
      <div className="flex justify-center items-center py-4 border-t border-gray-400">
        <p className="text-xs text-center">
          Don’t have an account?{" "}
          <Link
            className="underline underline-margin text-pumpkin-orange"
            to="/"
          >
            Register
          </Link>
        </p>
      </div>
    </div>
  );
};
