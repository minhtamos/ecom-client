import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export const ProductSideOthers = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch("/api/product/others").then((res) => {
      res.json().then((resJSON) => {
        setProducts(resJSON.products);
      });
    });
  }, []);

  return (
    <div className="overflow-scroll mr-4 h-detail-img">
      <h3 className="font-medium text-dark-grey">More from</h3>
      <h5 className="text-greyish-brown">Zara</h5>
      {products.map((product, i) => {
        return (
          <Link
            key={product.friendlyUrl}
            className="focus:outline-none block sub-img-container"
            to={`/detail/${product.friendlyUrl}`}
          >
            <img
              src={product.images[0]}
              alt="proImg"
              key={i}
              className="image-hover w-detail-sub-img h-side-others object-cover"
            />
          </Link>
        );
      })}
    </div>
  );
};
