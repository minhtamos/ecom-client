import React from "react";

export const SubImg = ({ images, setMainImg }) => {
  return (
    <div className="overflow-scroll mr-4 object-cover w-detail-sub-img h-detail-img">
      {images.map((img, i) => {
        return (
          <button key={`${img}-${i}`} onClick={() => setMainImg(img)} className="focus:outline-none block sub-img-container">
            <img src={img} key={i} className="image-hover w-detail-sub-img" alt="sub"/>
          </button>
        );
      })}
    </div>
  );
};
