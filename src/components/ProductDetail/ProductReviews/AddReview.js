import React, { useState } from "react";
import { useSelector } from "react-redux";
import { BeatLoader } from "react-spinners";
import classnames from "classnames";
import { SelectStarRating } from "./AddReview/SelectStarRating";

export const AddReview = ({ proName }) => {
  const [title, setTitle] = useState("");
  const [cmt, setCmt] = useState("");
  const [rating, setRating] = useState(0);
  const [loading, setLoading] = useState(false);
  const [msg, setMsg] = useState({
    isErr: false,
    text: "We love to hear your feedback",
  });
  const user = useSelector((state) => state.user);

  const submitReview = (e) => {
    e.preventDefault();
    setLoading(true);
    const body = {
      proName: proName,
      userName: user.name,
      userEmail: user.email,
      reviewedAt: new Date(),
      title: title,
      cmt: cmt,
      rating: rating,
    };
    fetch("/api/product/review", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    }).then((res) => {
      res.json().then((resJSON) => {
        if (resJSON.success) {
          setMsg({ isErr: false, text: "Reviewed successful!" });
        } else {
          setMsg({ isErr: true, text: resJSON.error.name });
        }
      });
      setLoading(false);
    });
  };

  return (
    <form className="flex mt-4" onSubmit={submitReview}>
      <div className="w-48">
        <h5 className="text-dark-grey font-bold">You</h5>
      </div>
      <div className="w-full p-4 bg-f9">
        <input
          type="text"
          className="w-full py-2 font-medium focus:outline-none px-4 shadow-sm"
          placeholder="TITLE"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <textarea
          className="w-full my-2 py-2 px-4 text-xs focus:outline-none shadow-sm"
          placeholder="Add your comment here…"
          rows={5}
          value={cmt}
          onChange={(e) => setCmt(e.target.value)}
        />
        <div className="flex justify-between items-center">
          <div>
            <h5 className="text-box text-xs">*Rating for us:</h5>
            <SelectStarRating rating={rating} setRating={setRating} />
          </div>
          <div className="flex">
            {loading ? (
              <BeatLoader color="#ff6900" />
            ) : (
              <p
                className={classnames(
                  msg.isErr ? "text-red-600" : "text-green-600"
                )}
              >
                {msg.text}
              </p>
            )}
            <button
              className="text-semibold text-white py-2 px-8 focus:outline-none bg-orange-400 hover:bg-orange-500 active:bg-orange-300 ml-4"
              onClick={submitReview}
            >
              Submit
            </button>
          </div>
        </div>
      </div>
    </form>
  );
};
