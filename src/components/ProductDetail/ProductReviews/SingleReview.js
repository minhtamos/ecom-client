import React from "react";
import { StarsRating } from "../../common";

export const SingleReview = ({ review }) => {
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const reviewedDate = new Date(review.reviewedAt);
  const dateReviewed = `${reviewedDate.getDate()} ${
    monthNames[reviewedDate.getMonth() - 1].slice(0, 3)
  }`;
  return (
    <div className="flex mt-4">
      <div className="w-48">
        <h5 className="text-dark-grey font-bold">{review.userName}</h5>
        <p className="text-black-grey">{dateReviewed}</p>
      </div>
      <div className="w-full p-4 bg-f9">
        <h5 className="text-dark-grey font-bold">{review.title}</h5>
        <StarsRating rating={4} />
        <p className="mt-4 text-greyish-brown">{review.cmt}</p>
      </div>
    </div>
  );
};
