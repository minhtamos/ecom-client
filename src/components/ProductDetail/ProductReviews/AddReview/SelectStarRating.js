import React from "react";
import { StarIcon, StarActiveIcon } from "../../../icons";

export const SelectStarRating = ({ rating, setRating }) => {
  const stars = ["star1", "star2", "star3", "star4", "star5"];
  return (
    <div className="grid grid-cols-5 gap-2 w-24">
      {stars.map((star, index) => {
        return index + 1 <= rating ? (
          <button
            type="button"
            className="focus:outline-none"
            onClick={() => setRating(index + 1)}
            key={`star-${star}`}
          >
            <StarActiveIcon />
          </button>
        ) : (
          <button
            type="button"
            className="focus:outline-none"
            onClick={() => setRating(index + 1)}
            key={`star-${star}`}
          >
            <StarIcon />
          </button>
        );
      })}
    </div>
  );
};
