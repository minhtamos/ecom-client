import { ProductReviews } from "./ProductReviews";
import { LineTitle } from "./LineTitle";
import { SelectColor } from "./SelectColor";
import { SelectSize } from "./SelectSize";
import { SubImg } from "./SubImg";
import { ProductSideOthers } from "./ProductSideOthers";
import { ProductOthers } from "./ProductOthers";

export {
  ProductReviews,
  LineTitle,
  SelectColor,
  SelectSize,
  SubImg,
  ProductSideOthers,
  ProductOthers,
};
