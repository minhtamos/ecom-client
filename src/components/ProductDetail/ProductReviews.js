import React from "react";
import { ArrowIcon } from "../icons";
import { LineTitle } from "./LineTitle";
import { SingleReview } from "./ProductReviews/SingleReview";
import { AddReview } from "./ProductReviews/AddReview";
import { useSelector } from "react-redux";

export const ProductReviews = ({ reviews, proName }) => {
  const authenticated = useSelector((state) => state.user.authenticated);
  return (
    <div className="mb-16">
      <LineTitle title="Reviews" />
      <div className="px-16">
        {authenticated ? (
          <AddReview proName={proName} />
        ) : (
          <h3 className="text-center text-red-600 italic">
            Please login to review
          </h3>
        )}
        {reviews.length === 0 ? (
          <p className="text-gray-600 italic mt-8">No review</p>
        ) : (
          reviews.map((review) => {
            return (
              <SingleReview
                review={review}
                key={`${review.userEmail}-${proName}`}
              />
            );
          })
        )}
        <div className="flex justify-end mt-16">
          <div className="flex justify-center items-center">
            <button className="focus:outline-none" onClick={() => {}}>
              <ArrowIcon left />
            </button>
            <p className="font-medium mx-4">1</p>
            <button className="focus:outline-none" onClick={() => {}}>
              <ArrowIcon right />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
