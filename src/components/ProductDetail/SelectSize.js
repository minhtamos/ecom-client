import React, { useEffect } from "react";
import classnames from "classnames";
import { sizesDefault } from "../../constants";

const SelectSizeBox = ({ title, active, disabled, normal, setSelectedSize }) => {
  return (
    <button
      className={classnames(
        {
          "bg-pale-orange font-bold text-white": active,
          "border-white-four text-white-four pointer-event-none cursor-default": disabled,
          "border-box text-dark-grey hover:bg-pale-orange hover:text-white": normal,
        },
        "h-10 w-10 flex justify-center items-center border focus:outline-none uppercase"
      )}
      disabled={disabled}
      onClick={() => setSelectedSize(title)}
    >
      {title}
    </button>
  );
};

export const SelectSize = ({ sizes, selectedSize, setSelectedSize }) => {

  useEffect(() => {
    setSelectedSize(sizes[0]);
  }, [setSelectedSize, sizes]);

  return (
    <div className="py-4 grid grid-cols-3 gap-2 border-b border-pinkish-grey w-40">
      {sizesDefault.map((size) => {
        return (
          <SelectSizeBox
            title={size}
            disabled={!sizes.includes(size)}
            active={selectedSize === size}
            normal={selectedSize !== size && sizes.includes(size)}
            setSelectedSize={setSelectedSize}
            key={`box-${size}`}
          />
        );
      })}
    </div>
  );
};
