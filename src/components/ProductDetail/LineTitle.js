import React from "react";

export const LineTitle = ({title}) => {
  return (
    <div className="flex items-center my-4">
      <div className="w-16 border-b border-line"></div>
      <p className="mx-8 font-medium text-dark-grey whitespace-no-wrap">{title}</p>
      <div className="w-full border-b border-line"></div>
    </div>
  );
};
