import React, {useState, useEffect} from "react";
import { LineTitle } from "./LineTitle";
import { Link } from "react-router-dom";

export const ProductOthers = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch("/api/product/others").then((res) => {
      res.json().then((resJSON) => {
        setProducts(resJSON.products);
      });
    });
  }, []);

  return (
    <div className="mb-16">
      <LineTitle title="You may also like" />
      <div className="grid grid-cols-8 gap-4 text-xs font-medium">
        {products.map((product) => {
          return (
            <Link to={`/detail/${product.friendlyUrl}`} key={`others-${product.friendlyUrl}`}>
              <img
                src={product.images[0]}
                alt="thump"
                className="h-others object-cover w-full"
              />
              <h5 className="text-greyish-brown">{product.name}</h5>
            </Link>
          );
        })}
      </div>
    </div>
  );
};
