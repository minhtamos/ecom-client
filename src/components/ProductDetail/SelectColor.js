import React from "react";
import classnames from "classnames";
import { CheckWhiteIcon } from "../icons";

export const SelectColor = ({ oneline, colors, selectedColor, setSelectedColor }) => {
  return (
    <div
      className={classnames("grid py-4", {
        "grid-cols-5 w-64": oneline,
        "grid-cols-4 w-48": !oneline,
      })}
    >
      {colors.map((color) => {
        return (
          <button
            className="rounded-full w-8 h-8 flex justify-center items-center focus:outline-none"
            style={{ backgroundColor: color }}
            key={color}
            onClick={() => setSelectedColor(color)}
          >
            {selectedColor === color ? <CheckWhiteIcon /> : <></>}
          </button>
        );
      })}
    </div>
  );
};
