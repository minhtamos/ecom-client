import React from "react";
import { ArrowIcon } from "../icons";
import { NavDropDown } from "./NavDropDown";
import { Link } from "react-router-dom";

export const NavLink = ({ nav }) => {
  return (
    <div className="group relative flex items-center justify-center">
      <Link className="flex items-center justify-center text-xs font-medium mx-2 hover:bg-white-three px-2 py-1 " to={`list?character=${nav}`}>
        {nav}
        <ArrowIcon down />
      </Link>
      <NavDropDown nav={nav} />
    </div>
  );
};
