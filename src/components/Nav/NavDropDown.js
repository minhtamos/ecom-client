import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

export const NavDropDown = ({ nav }) => {
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    fetch(`/api/category?character=${nav}`).then((res) => {
      res.json().then((resJSON) => {
        if (resJSON.success) {
          setCategories(resJSON.categories);
        }
      });
    });
  }, [nav]);
  return (
    <div className="absolute top-nav z-10 pt-4 flex opacity-0 group-hover:opacity-100 pointer-events-none group-hover:pointer-events-auto
      transform translate-y-6 group-hover:translate-y-0 duration-300
    ">
      <div className="flex border shadow" style={{ borderColor: "#eaeaea" }}>
        {categories.map((cate) => {
          return (
            <NavLink
              className="py-3 px-8 bg-fb text-dark-grey whitespace-no-wrap hover:bg-gray-200 text-xs"
              key={cate.name}
              to={`/list?page=1&category=${cate.name}&character=${nav}`}
            >
              {cate.name}
            </NavLink>
          );
        })}
      </div>
    </div>
  );
};
