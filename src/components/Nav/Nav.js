import React from "react";
import { navs } from "../../constants";
import { NavLink } from "./NavLink";
import { Link } from "react-router-dom";

export const Nav = () => {
  return (
    <nav className="block p-2 shadow border border-gray-200 sticky top-0 bg-white z-10 relative ">
      <div className="inline-block flex justify-center">
        <div className="flex justify-center items-center">
          <Link
            className="flex items-center justify-center text-xs font-medium mx-2 hover:bg-white-three px-2 py-1 group relative"
            to="/list"
          >
            ALL
          </Link>
          {navs.map((nav) => {
            return <NavLink key={nav} nav={nav} />;
          })}
          <Link
            className="flex items-center justify-center text-xs font-medium mx-2 hover:bg-white-three px-2 py-1 group relative"
            to="/orders"
          >
            Orders
          </Link>
        </div>
      </div>
    </nav>
  );
};
