import React from "react";
import { LogoIcon } from "../icons/LogoIcon";
import { footerNavs } from "../../constants";
import { TwitterIcon, FacebookIcon, InstagramIcon } from "../icons";

const HomeFooterStyles = {
  links: {
    color: "#b7b7b7",
    fontSize: "14px",
  },
};

export const Footer = () => {
  return (
    <footer className="px-16 border-t border-gray-300 shadow-lg">
      <div className="flex justify-between items-center py-8">
        <LogoIcon />
        <div className="flex">
          {footerNavs.map((nav) => {
            return (
              <div
                className="mx-2"
                style={HomeFooterStyles.links}
                key={`footer1-${nav}`}
              >
                {nav}
              </div>
            );
          })}
        </div>
        <div className="grid grid-cols-3 gap-3">
          <TwitterIcon />
          <FacebookIcon />
          <InstagramIcon />
        </div>
      </div>
      <div className="flex border-t border-gray-200 justify-between py-4">
        <div className="flex">
          {footerNavs.map((nav) => {
            return (
              <div
                className="mx-2"
                style={HomeFooterStyles.links}
                key={`footer2-${nav}`}
              >
                {nav}
              </div>
            );
          })}
        </div>
        <div className="flex">
          <div className="mx-2" style={HomeFooterStyles.links}>
            Privacy Policy
          </div>
          <div className="mx-2" style={HomeFooterStyles.links}>
            Terms & Conditions
          </div>
        </div>
      </div>
    </footer>
  );
};
