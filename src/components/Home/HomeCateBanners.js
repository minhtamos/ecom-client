import React from "react";
import { Link } from "react-router-dom";
import { cateBanners } from "../../constants";

const HomeCateBanner = ({ link, title }) => {
  return (
    <div
      className="flex flex-col justify-end p-8 items-center bg-cover h-cate-banner bg-gray-600"
      style={{ backgroundImage: `url(${link})` }}
    >
      <h3 className="font-bold domine-font text-2xl text-white">{title}</h3>
      <div className="w-full border-b border-ea"></div>
      <Link
        className="py-2 mt-4 text-white focus:outline-none w-2/3 bg-pale-orange font-semibold flex justify-center items-center hover:bg-orange-500"
        to="/list"
      >
        Shop now
      </Link>
    </div>
  );
};

export const HomeCateBanners = () => {
  return (
    <div className="grid grid-cols-4 px-16 pt-4 pb-16 gap-4">
      {cateBanners.map((banner) => {
        return (
          <HomeCateBanner
            title={banner.title}
            link={banner.link}
            key={`catebanner-${banner.title}`}
          />
        );
      })}
    </div>
  );
};
