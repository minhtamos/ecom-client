import React from "react";
import { Link } from "react-router-dom";
import "./Home.css";

export const HomeBanner = () => {
  return (
    <div className="px-16 mt-8">
      <div className="flex flex-col justify-between home-banner-image h-banner bg-cover home-banner-container bg-gray-600">
        <div className="flex justify-end">
          <h1 className="uppercase font-bold domine-font text-white text-5xl">
            Outfit of the week
          </h1>
        </div>
        <div className="flex justify-end">
          <Link
            className="px-8 py-3 rounded text-white font-bold focus:outline-none bg-pale-orange home-banner-container-link hover:bg-orange-500"
            to="/list"
          >
            Shop now
          </Link>
        </div>
      </div>
    </div>
  );
};
