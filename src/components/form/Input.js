import React from "react";
import classnames from "classnames";
import "./Input.css";

export const Input = ({
  title,
  type,
  placeholder,
  error,
  value,
  onChange,
  borderClass,
  bgClass,
  labelClass,
}) => {
  return (
    <div className="my-3 text-xs w-full">
      <label
        className={classnames(
          "block font-bold mb-2 tracking-wide",
          labelClass ? labelClass : "text-dark-grey"
        )}
      >
        {title}
      </label>
      <input
        type={type}
        className={classnames(
          "px-4 py-3 w-full focus:outline-none border shadow rounded-sm",
          error.isError ? "bg-input-error" : bgClass || "bg-white-two",
          error.isError
            ? "border-strawberry"
            : borderClass || "border-greyish-two"
        )}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
      />
      <p
        className={classnames(
          "text-2xs min-height-error",
          error.isError ? "text-strawberry" : "text-black-grey"
        )}
      >
        {error.msg}
      </p>
    </div>
  );
};
