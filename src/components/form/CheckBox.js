import React from "react";
import { CheckIcon } from "../icons";

export const CheckBox = ({ checked, toggle, title }) => {
  return (
    <label className="checkbox-container flex items-center justify-center">
      <input
        type="checkbox"
        className="border border-gray-200"
        checked={checked}
        onChange={() => {
          toggle(!checked);
        }}
      />
      {checked ? <CheckIcon /> : <span className="checkmark"></span>}
      {title}
    </label>
  );
};
