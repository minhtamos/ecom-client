import React from "react";

export const FormBtn = ({ title }) => {
  return (
    <input
      type="submit"
      value="Submit"
      className="w-full p-3 my-4 shadow-lg focus:outline-none bg-pale-orange text-white hover:bg-orange-500 cursor-pointer"
      title={title}
    />
  );
};
