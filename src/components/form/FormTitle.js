import React from "react";

export const FormTitle = ({ title }) => {
  return (
    <p
      className="text-2xl text-center font-bold p-2 text-dark-grey"
    >
      {title}
    </p>
  );
};
