import React from "react";

export const EditIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="24"
      height="24"
      viewBox="0 0 24 24"
    >
      <defs>
        <path
          id="edit"
          d="M4 16.854v2.702a.44.44 0 0 0 .444.444h2.702a.417.417 0 0 0 .311-.133l9.706-9.697-3.333-3.333-9.697 9.697a.436.436 0 0 0-.133.32zm15.74-9.261a.885.885 0 0 0 0-1.253l-2.08-2.08a.885.885 0 0 0-1.253 0l-1.626 1.626 3.333 3.333 1.626-1.626z"
        />
      </defs>
      <use fill="#9B9B9B" fillRule="evenodd" xlinkHref="#edit" />
    </svg>
  );
};
