import { ArrowIcon } from "./ArrowIcon";
import { CartIcon } from "./CartIcon";
import { CheckIcon } from "./CheckIcon";
import { FacebookIcon } from "./FacebookIcon";
import { InstagramIcon } from "./InstagramIcon";
import { LogoIcon } from "./LogoIcon";
import { SearchIcon } from "./SearchIcon";
import { TwitterIcon } from "./TwitterIcon";
import { StarIcon } from "./StarIcon";
import { PlusIcon } from "./PlusIcon";
import { MinusIcon } from "./MinusIcon";
import { StarActiveIcon } from "./StarActiveIcon";
import { CheckWhiteIcon } from "./CheckWhiteIcon";
import { OverviewIcon } from "./OverviewIcon";
import { OrdersIcon } from "./OrdersIcon";
import { ProductsIcon } from "./ProductsIcon";
import { PaymentIcon } from "./PaymentIcon";
import { PromotionIcon } from "./PromotionIcon";
import { SettingIcon } from "./SettingIcon";
import { DropDownIcon } from "./DropDownIcon";
import { MailIcon } from "./MailIcon";
import { BellIcon } from "./BellIcon";
import { AdminSearchIcon } from "./AdminSearchIcon";
import { PlusWhiteIcon } from "./PlusWhiteIcon";
import { EditIcon } from "./EditIcon";
import { TrashIcon } from "./TrashIcon";
import { LastPageIcon } from "./LastPageIcon";
import { FirstPageIcon } from "./FirstPageIcon";
import { NextIcon } from "./NextIcon";
import { PrevIcon } from "./PrevIcon";
import { ExportIcon } from "./ExportIcon";
import { CloseIcon } from "./CloseIcon";
import { AddIcon } from "./AddIcon";
import { CrossIcon } from "./CrossIcon";
import { Close1 } from "./Close1";

export {
  ArrowIcon,
  CartIcon,
  CheckIcon,
  FacebookIcon,
  InstagramIcon,
  LogoIcon,
  SearchIcon,
  TwitterIcon,
  StarIcon,
  PlusIcon,
  MinusIcon,
  StarActiveIcon,
  CheckWhiteIcon,
  OverviewIcon,
  OrdersIcon,
  ProductsIcon,
  PaymentIcon,
  PromotionIcon,
  SettingIcon,
  DropDownIcon,
  MailIcon,
  BellIcon,
  AdminSearchIcon,
  PlusWhiteIcon,
  EditIcon,
  TrashIcon,
  FirstPageIcon,
  LastPageIcon,
  NextIcon,
  PrevIcon,
  ExportIcon,
  CloseIcon,
  AddIcon,
  CrossIcon,
  Close1,
};
