import React, { useState, useRef } from "react";
import classnames from "classnames";
import { ArrowIcon } from "../icons/ArrowIcon";
import { useOnClickOutSide } from "../../hooks";
import { CheckWhiteIcon } from "../icons";

const getTitle = (sortBy) => {
  if (sortBy === "name") {
    return "Name: A - Z";
  }
  if (sortBy === "price-asc") {
    return "Price: lowest to highest";
  }
  if (sortBy === "price-desc") {
    return "Price: highest to lowest";
  }
  return "";
};

export const ProductSortBy = ({ selectedSortBy, setSortBy }) => {
  const ops = [
    {
      title: "Name: A - Z",
      value: "name-asc",
    },
    {
      title: "Name: Z - A",
      value: "name-desc",
    },
    {
      title: "Price: lowest to highest",
      value: "price-asc",
    },
    {
      title: "Price: highest to lowest",
      value: "price-desc",
    },
  ];
  const [show, setShow] = useState(false);
  const ref = useRef();
  useOnClickOutSide(ref, () => {
    setShow(false);
  });

  const handleClickSortBy = (op) => {
    if (selectedSortBy.includes(op.value)) {
      setSortBy(selectedSortBy.filter((sb) => sb !== op.value));
    } else {
      const [sortBy, sort] = op.value.split("-");
      const opositeSortby = `${sortBy}-${sort === "asc" ? "desc" : "asc"}`;
      if (selectedSortBy.includes(opositeSortby)) {
        setSortBy([
          ...selectedSortBy.filter((value) => value !== opositeSortby),
          op.value,
        ]);
      } else {
        setSortBy([...selectedSortBy, op.value]);
      }
    }
  };

  return (
    <div className="relative" ref={ref}>
      <button
        className="flex justify-around w-56 items-center px-2 py-1 border border-gray-300"
        onClick={() => setShow(!show)}
      >
        <p className="font-medium text-greyish-brown text-xs">
          Sort By: {selectedSortBy.length} selected
        </p>
        <p className="font-bold">{getTitle(selectedSortBy)}</p>
        <ArrowIcon down />
      </button>
      <div
        className={classnames(
          "absolute z-20 bg-white w-full",
          show ? "block" : "hidden"
        )}
      >
        {ops.map((op, index) => {
          return (
            <button
              className="flex justify-between w-full items-center px-4 py-2 border-b border-l border-r border-white-five hover:bg-gray-200 text-greyish-brown text-xs h-8"
              onClick={() => handleClickSortBy(op)}
              key={op.value}
            >
              <p className="font-medium">{op.title}</p>
              {selectedSortBy.includes(op.value) ? <CheckWhiteIcon /> : ""}
            </button>
          );
        })}
      </div>
    </div>
  );
};
