import React from "react";
import { FilterCheckBox } from "./FilterCheckBox";

export const FilterBrand = ({ brands, selectedBrands, setBrands }) => {
  return (
    <div className="p-2">
      {brands.map((brand) => {
        return (
          <FilterCheckBox
            title={brand}
            key={brand}
            selectedOps={selectedBrands}
            setOps={setBrands}
          />
        );
      })}
    </div>
  );
};
