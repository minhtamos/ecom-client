import React from "react";
import InputRange from "react-input-range";
import { priceRange } from "../../../../constants";
import "react-input-range/lib/css/index.css";

export const FilterPrice = ({ priceFilter, setPriceFilter, setPrice }) => {
  return (
    <div className="py-8">
      <InputRange
        maxValue={priceRange.max}
        minValue={priceRange.min}
        value={priceFilter}
        onChange={(value) => setPriceFilter(value)}
        onChangeComplete={(value) => {
          setPrice(value);
        }}
      />
    </div>
  );
};
