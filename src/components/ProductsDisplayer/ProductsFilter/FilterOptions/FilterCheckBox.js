import React from "react";
import { CheckBox } from "../../../form";

export const FilterCheckBox = ({ title, selectedOps, setOps }) => {
  return (
    <div className="p-2 my-2 text-greyish-brown flex justify-between bg-fa">
      <p style={{ color: selectedOps.includes(title) ? "#ff6900" : "#4d4d4d" }}>
        {title}
      </p>
      <CheckBox
        checked={selectedOps.includes(title)}
        toggle={() => {
          if (selectedOps.includes(title)) {
            setOps(selectedOps.filter((op) => op !== title));
          } else {
            setOps([...selectedOps, title]);
          }
        }}
      />
    </div>
  );
};
