import React from "react";
import { FilterCheckBox } from "./FilterCheckBox";

export const FilterAvailable = ({ selectedAvailable, setAvailable }) => {
  return (
    <div className="p-2">
      <FilterCheckBox
        title="In-store"
        selectedOps={selectedAvailable}
        setOps={setAvailable}
      />
      <FilterCheckBox
        title="Out of stock"
        selectedOps={selectedAvailable}
        setOps={setAvailable}
      />
    </div>
  );
};
