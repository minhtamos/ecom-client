import React from "react";
import classnames from "classnames";
import { sizesDefault } from "../../../../constants";

const FilterSizeBox = ({
  title,
  active,
  disabled,
  normal,
  setSizes,
  selectedSizes,
}) => {
  return (
    <button
      className={classnames(
        {
          "bg-pale-orange font-bold text-white": active,
          "border-white-four text-white-four pointer-event-none cursor-default": disabled,
          "border-box text-dark-grey hover:bg-pale-orange hover:text-white": normal,
        },
        "h-10 w-10 flex justify-center items-center border focus:outline-none uppercase"
      )}
      disabled={disabled}
      onClick={() => {
        if (!active) {
          setSizes([...selectedSizes, title]);
        } else {
          setSizes(selectedSizes.filter((size) => size !== title));
        }
      }}
    >
      {title}
    </button>
  );
};

export const FilterSize = ({ sizes, selectedSizes, setSizes }) => {
  return (
    <div className="py-4 grid grid-cols-3 gap-2 border-b border-pinkish-grey w-40">
      {sizesDefault.map((size) => {
        return (
          <FilterSizeBox
            title={size}
            disabled={!sizes.includes(size)}
            active={selectedSizes.includes(size)}
            normal={!selectedSizes.includes(size) && sizes.includes(size)}
            setSizes={setSizes}
            selectedSizes={selectedSizes}
            key={`box-${size}`}
          />
        );
      })}
    </div>
  );
};
