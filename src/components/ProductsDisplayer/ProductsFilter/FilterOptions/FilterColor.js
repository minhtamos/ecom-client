import React from "react";
import classnames from "classnames";
import {CheckWhiteIcon} from "../../../icons"

export const FilterColor = ({ colors, setColors, selectedColors }) => {
  return (
    <div className={classnames("grid py-4 grid-cols-4 w-48 gap-4")}>
      {colors.map((color) => {
        return (
          <button
            className="rounded-full w-8 h-8 shadow border border-white-two flex justify-center items-center"
            style={{ backgroundColor: color }}
            key={color}
            onClick={() => {
              if (selectedColors.includes(color)) {
                setColors(selectedColors.filter((c) => c !== color));
              } else {
                setColors([...selectedColors, color]);
              }
            }}
          >
            {selectedColors.includes(color) ? <CheckWhiteIcon /> : <></>}
          </button>
        );
      })}
    </div>
  );
};
