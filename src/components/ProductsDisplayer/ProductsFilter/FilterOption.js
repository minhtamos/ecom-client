import React, { useState } from "react";
import classnames from "classnames";
import { ArrowIcon } from "../../icons";

export const FilterOption = ({ children, title }) => {
  const [active, setActive] = useState(false);
  
  return (
    <div className="mr-8">
      <button
        className={classnames(
          "py-2 w-full flex justify-between border-b border-pinkish-grey focus:outline-none text-sm text-dark-grey",
          { "border-dashed": active }
        )}
        onClick={() => setActive(!active)}
      >
        <p>{title}</p>
        {active ? <ArrowIcon down /> : <ArrowIcon up />}
      </button>
      {active ? children : ""}
    </div>
  );
};
