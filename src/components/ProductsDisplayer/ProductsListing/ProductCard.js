import React from "react";
import { Link } from "react-router-dom";
import { isSoldOut } from "../../../utils";

const ProductCardStyles = {
  name: {
    color: "#202124",
    fontSize: "14px",
    fontWeight: "500",
  },
  price: {
    color: "#4d4d4d",
    fontSize: "12px",
  },
  image: {
    height: "15rem",
  },
  soldOut: {
    width: "60px",
    height: "22px",
    backgroundColor: "#808080",
    fontSize: "12px",
    fontWeight: "bold",
    fontStretch: " normal",
    fontStyle: "normal",
    lineHeight: "1.5",
    letterSpacing: "normal",
    color: "#ffffff",
    left: "-10px",
    bottom: "10px",
  },
  quickShop: {
    backgroundColor: "#ffa15f",
    color: "white",
  },
};

export const ProductCard = ({ product }) => {
  const { name, price, images, quantities, friendlyUrl, solds } = product;
  return (
    <div className="group">
      <div className="relative">
        <div style={ProductCardStyles.image} className="bg-gray-600">
          <img
            src={images[0]}
            alt="thump"
            className="object-cover w-full"
            style={ProductCardStyles.image}
          />
        </div>
        {isSoldOut(quantities, solds) ? (
          <div
            className="absolute bottom-0 left-0 flex justify-center items-center"
            style={ProductCardStyles.soldOut}
          >
            Sold out
          </div>
        ) : (
          ""
        )}
        <Link
          className="absolute bottom-0 w-full flex justify-center items-center py-4 transition-all
          opacity-0 group-hover:opacity-100
          transform translate-y-4 duration-300 group-hover:translate-y-0"
          style={ProductCardStyles.quickShop}
          to={`/detail/${friendlyUrl}`}
        >
          + Quick shop
        </Link>
      </div>
      <p style={ProductCardStyles.name} className="mt-1">
        {name}
      </p>
      <p style={ProductCardStyles.price} className="mt-1">
        ${price}
      </p>
    </div>
  );
};
