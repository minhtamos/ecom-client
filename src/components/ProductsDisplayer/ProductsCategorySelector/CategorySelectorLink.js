import React from "react";
import classnames from "classnames";

export const CategorySelectorLink = ({ active, title, setCategory }) => {
  return (
    <>
      <button
        className={classnames(
          "py-2 text-sm",
          active ? "text-bright-orange" : "text-greyish-brown"
        )}
        onClick={() => setCategory(title)}
      >
        {title}
      </button>
      <div
        className={classnames({ "w-8 border-b border-bright-orange": active })}
      ></div>
    </>
  );
};
