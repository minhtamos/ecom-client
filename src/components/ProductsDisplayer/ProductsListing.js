import React from "react";
import { ProductCard } from "./ProductsListing/ProductCard";

const ProductsListing = ({ products }) => {
  return (
    <div className="grid grid-cols-5 gap-8">
      {products.map((product) => {
        return <ProductCard product={product} key={product._id} />;
      })}
    </div>
  );
};

export default ProductsListing;
