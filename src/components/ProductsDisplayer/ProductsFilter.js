import React from "react";
import {
  FilterSize,
  FilterColor,
  FilterBrand,
  FilterAvailable,
  FilterPrice,
} from "./ProductsFilter/FilterOptions";
import { FilterOption } from "./ProductsFilter/FilterOption";
import { brands, colors } from "../../constants";

const ProductsFilter = () => {
  return (
    <aside className="mt-8">
      <p className="font-bold mb-8">Filter</p>
      <FilterOption title="Size">
        <FilterSize sizes={["m", "l", "h"]} setSelectedSize={() => {}} />
      </FilterOption>
      <FilterOption title="Color">
        <FilterColor colors={colors} />
      </FilterOption>
      <FilterOption title="Brand">
        <FilterBrand brands={brands} />
      </FilterOption>
      <FilterOption title="Price">
        <FilterPrice />
      </FilterOption>
      <FilterOption title="Available">
        <FilterAvailable />
      </FilterOption>
    </aside>
  );
};

export default ProductsFilter;
