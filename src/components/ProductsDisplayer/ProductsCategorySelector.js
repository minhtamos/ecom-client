import React, { useState, useEffect } from "react";
import { CategorySelectorLink } from "./ProductsCategorySelector/CategorySelectorLink";

const ProductsCategorySelector = ({
  selectedCategory,
  setCategory,
  character,
}) => {
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    const url = `/api/category${
      character === "" ? "" : `?character=${character}`
    }`;
    fetch(url).then((res) => {
      res.json().then((resJSON) => {
        if (resJSON.success) {
          setCategories(resJSON.categories);
        }
      });
    });
  }, [character]);
  return (
    <aside className="w-56">
      <p className="font-bold mb-8">{character === "" ? "Catergory" : `Category for ${character}`}</p>
      <CategorySelectorLink
        title="All"
        active={"All" === selectedCategory}
        setCategory={setCategory}
      />
      {categories.map((cate) => {
        return (
          <CategorySelectorLink
            title={cate.name}
            key={cate.name}
            active={cate.name === selectedCategory}
            setCategory={setCategory}
          />
        );
      })}
    </aside>
  );
};

export default ProductsCategorySelector;
