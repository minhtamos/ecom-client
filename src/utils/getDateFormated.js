import { days, months } from "../constants";

export const getDateFormated = (dateString) => {
  const date = new Date(dateString);
  const day = date.getDay();
  const month = date.getMonth();
  const year = date.getYear();
  return `${days[day]}, ${day}th ${months[month]}, ${year}`;
};
