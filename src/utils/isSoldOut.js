export const isSoldOut = (quantities, solds) => {
  for (let i = 0; i < quantities.length; i++) {
    if (quantities[i] - solds[i] !== 0) {
      return false;
    }
  }
  return true;
};
